import java.io.*;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Record record = new Record();
        record.setPhoneNumber("123456");
        record.setFirstName("Sergey");
        record.setSecondName("Altuchov");
        record.setSurname("Vladimirovich");
        record.createFullName();
        record.setAddres("Dnepr");
        PhoneBook pb = new PhoneBook();
        pb.addRecord(record);
        //phoneBook = pb;
        //System.out.println(phoneBook.toString());
        //writeToFile();
        //readFromFile();
        //System.out.println(phoneBook.toString());

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String wh = "";
        System.out.println("--- Start of the program ---");
        do {
            System.out.print("Enter the command: ");
            String result = reader.readLine();
            if (result.equals("exit")){
                writeToFile();
                wh = result;
            }
            else if (result.equals("print")){
                if ( phoneBook != null) {
                    System.out.println(phoneBook.toString());
                }else {
                    System.out.println("=== Have not eny somthing!!! ===");
                }
            }else if (result.equals("add")){
                System.out.println("Creating new record:");
                createRecord(reader);
            }else if (result.equals("update")){
                System.out.println("Update record with number: ");
                updateRecord(reader);
            }else if (result.contains("remove")){
                System.out.println("Remove record with number: ");
                removeFromPhoneBook(reader);
            }else if (result.equals("find")){
                System.out.println("Find record by (1. Name, 2. PhoneNumber, 3. Address): ");
                findByRecord(reader);
            }
        }while (!wh.contains("exit"));
    }

    static PhoneBook phoneBook = null;

    public static void writeToFile() throws IOException {
        File fl = new File("phoneBook.txt");
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fl));
        if (phoneBook != null) {
            try {
                oos.writeObject(phoneBook);
            } finally {
                oos.flush();
                oos.close();
            }
        }else {
            try {
                PhoneBook pb = new PhoneBook();
                oos.writeObject(pb);
            } finally {
                oos.flush();
                oos.close();
            }
        }
    }
    public static void readFromFile() throws IOException, ClassNotFoundException {
        File fl = new File("phoneBook.txt");
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fl));
        if (fl.exists()){
            try {
                phoneBook = (PhoneBook) ois.readObject();
            }finally {
                ois.close();
            }
        }else {
            PhoneBook pB = new PhoneBook();
            phoneBook = pB;
        }
    }
    public static void createRecord(BufferedReader reader) throws IOException {
        Record tmp = new Record();
        String res = "";
        System.out.println("Type First Name: ");
        if ((res = reader.readLine()) != null) {
            tmp.setFirstName(res);
        }
        System.out.println("Type Second Name: ");
        if ((res = reader.readLine()) != null) {
            tmp.setSecondName(res);
        }
        System.out.println("Type surname: ");
        if ((res = reader.readLine()) != null) {
            tmp.setSurname(res);
        }
        System.out.println("Type Phone number: ");
        if ((res = reader.readLine()) != null) {
            tmp.setPhoneNumber(res);
        }
        System.out.println("Type Address: ");
        if ((res = reader.readLine()) != null) {
            tmp.setAddres(res);
        }
        tmp.createFullName();
        if (phoneBook != null){
            if (phoneBook.contains(tmp)) {
                System.out.println("--- Such record already exists! ---");
            }else{
                phoneBook.addRecord(tmp);
                System.out.println("=== Add successful!!! ===");
            }
        }else{
            PhoneBook pb = new PhoneBook();
            pb.addRecord(tmp);
            phoneBook = pb;
            System.out.println("=== Add successful!!! ===");
        }
    }
    public static void updateRecord(BufferedReader reader) throws IOException {
        long index = Long.parseLong(reader.readLine());
        if (index != 0){
            if (phoneBook != null){
               Record tmp =  phoneBook.findByNumber(index);
                if (tmp != null) {
                    String res = null;
                    System.out.println("Type First Name: ");
                    if ((res = reader.readLine()) != null) {
                        phoneBook.findByNumber(index).setFirstName(res);
                    }
                    System.out.println("Type Second Name: ");
                    if ((res = reader.readLine()) != null) {
                        phoneBook.findByNumber(index).setSecondName(res);
                    }
                    System.out.println("Type surname: ");
                    if ((res = reader.readLine()) != null) {
                        phoneBook.findByNumber(index).setSurname(res);
                    }
                    System.out.println("Type Phone number: ");
                    if ((res = reader.readLine()) != null) {
                        phoneBook.findByNumber(index).setPhoneNumber(res);
                    }
                    System.out.println("Type Address: ");
                    if ((res = reader.readLine()) != null) {
                        phoneBook.findByNumber(index).setAddres(res);
                    }
                    phoneBook.findByNumber(index).createFullName();
                    System.out.println("--- Update Successful!!! ---");
                }else {
                    System.out.println("--- No record with such number! ---");
                }
            }
        }else {
            System.out.println("--- No record with such number! ---");
        }
    }
    public static void removeFromPhoneBook(BufferedReader reader) throws IOException {
        Long tmp = Long.parseLong(reader.readLine());
        if (tmp != 0){
            if (phoneBook != null){
                if (phoneBook.findByNumber(tmp) != null){
                    phoneBook.removeRecord(phoneBook.findByNumber(tmp));
                }else {
                    System.out.println("--- No record with such number! ---");
                }
            }else {
                System.out.println("--- PhoneBook is Empty ---");
            }
        }else {
            System.out.println("--- No record with such number! ---");
        }
    }
    public static void findByRecord(BufferedReader reader) throws IOException {
        int index = Integer.parseInt(reader.readLine());
        switch (index){
            case 1:
                if (phoneBook != null) {
                    System.out.println("Type Full Name: ");
                    List<Record> list1 = phoneBook.findByFullName(reader.readLine());
                    if (list1.size() != 0) {
                        System.out.println(list1);
                    }
                    System.out.println("--- Do you want save search result to file? (yes/no): ");
                    if (reader.readLine().equals("yes")) {
                        writeToFile();
                        System.out.println("--- Save successful! ---");
                    }
                }else{
                    System.out.println("--- Phone Book is empty! ---");
                }
                break;
            case 2:
                System.out.println("Type Phone Number: ");
                if (phoneBook != null) {
                    List<Record> list2 = phoneBook.findByPhoneNumber(reader.readLine());
                    if (list2.size() != 0) {
                        System.out.println(list2);
                    }
                    System.out.println("--- Do you want save search result to file? (yes/no): ");
                    if (reader.readLine().equals("yes")) {
                        writeToFile();
                        System.out.println("--- Save successful! ---");
                    }
                }else{
                    System.out.println("--- Phone Book is empty! ---");
                }
                break;
            case 3:
                if (phoneBook != null) {
                    System.out.println("Type Full Name: ");
                    List<Record> list3 = phoneBook.findByAddress(reader.readLine());
                    if (list3.size() != 0) {
                        System.out.println(list3);
                    }
                    System.out.println("--- Do you want save search result to file? (yes/no): ");
                    if (reader.readLine().equals("yes")) {
                        writeToFile();
                        System.out.println("--- Save successful! ---");
                    }
                }else{
                    System.out.println("--- Phone Book is empty! ---");
                }
                break;
            default:
                System.out.println("--- Entered wrong number!!! ---");
                break;
        }
    }
}