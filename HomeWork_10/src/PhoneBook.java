import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PhoneBook implements Serializable{
    static final long serialVersionUID = 42457L;
    private List<Record> records = new ArrayList<>();

    @Override
    public String toString() {
        return "PhoneBook{" +
                "records=" + records +
                '}';
    }
    public void addRecord(Record record){
        record.setNumber(records.size() + 1L);
        records.add(record);
    }
    public void removeRecord(Record record){
        int index = records.indexOf(record);
        records.remove(record);
        for (int i = index; i < records.size(); i++) {
            records.get(i).setNumber(records.get(i).getNumber() - 1);
        }
        System.out.println("=== Remove was successful!!! ===");
    }
    public boolean contains(Record record){
        if (records.contains(record)){
            return true;
        }else {
            return false;
        }
    }
    public Record findByNumber(Long number){
        for (Record pair : records){
            if (pair.getNumber() == number){
                return pair;
            }
        }
        return null;
    }
    public List<Record> findByFullName(String name){
        List<Record> list = new ArrayList<>();
        for (Record pair : records){
            if (pair.getFullName().contains(name)){
                list.add(pair);
            }
        }
        return list;
    }
    public List<Record> findByPhoneNumber(String phoneNumber){
        List<Record> list = new ArrayList<>();
        for (Record pair : records){
            if (pair.getPhoneNumber() == phoneNumber){
                list.add(pair);
            }
        }
        return list;
    }
    public List<Record> findByAddress(String address){
        List<Record> list = new ArrayList<>();
        for (Record pair : records){
            if (pair.getAddres().contains(address)){
                list.add(pair);
            }
        }
        return list;
    }

}