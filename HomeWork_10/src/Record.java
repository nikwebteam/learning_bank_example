
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

public class Record implements Serializable{
    static final long serialVersionUID = 42456L;
    transient private String firstName;
    transient private String secondName;
    transient private String surname;
    private Long number;
    private String fullName;
    private String PhoneNumber;
    private String addres;

    public String getFirstName() {return firstName;}

    public void setFirstName(String firstName) {this.firstName = firstName;}

    public String getSecondName() {return secondName;}

    public void setSecondName(String secondName) {this.secondName = secondName;}

    public String getSurname() {return surname;}

    public void setSurname(String surname) {this.surname = surname;}

    public Long getNumber() {return number;}

    public void setNumber(Long number) {this.number = number;}

    public String getFullName() {return fullName;}

    public String getPhoneNumber() {return PhoneNumber;}

    public void setPhoneNumber(String phoneNumber) {PhoneNumber = phoneNumber;}

    public String getAddres() {return addres;}

    public void setAddres(String addres) {this.addres = addres;}

    @Override
    public String toString() {
        return "Record{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", surname='" + surname + '\'' +
                ", number=" + number +
                ", fullName='" + fullName + '\'' +
                ", PhoneNumber='" + PhoneNumber + '\'' +
                ", addres='" + addres + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Record record = (Record) o;

        if (fullName != null ? !fullName.equals(record.fullName) : record.fullName != null) return false;
        if (PhoneNumber != null ? !PhoneNumber.equals(record.PhoneNumber) : record.PhoneNumber != null) return false;
        return !(addres != null ? !addres.equals(record.addres) : record.addres != null);

    }

    @Override
    public int hashCode() {
        int result = fullName != null ? fullName.hashCode() : 0;
        result = 31 * result + (PhoneNumber != null ? PhoneNumber.hashCode() : 0);
        result = 31 * result + (addres != null ? addres.hashCode() : 0);
        return result;
    }

    public void createFullName(){
        fullName = surname + " " + firstName + " " + secondName;
    }

    private void setFields(){
        String[] tmp = fullName.split(" ");
        surname = tmp[0];
        firstName = tmp[1];
        secondName = tmp[2];
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        setFields();
    }
}