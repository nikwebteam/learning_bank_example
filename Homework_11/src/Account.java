
public class Account {
    private Long balance;

    Account(Long balance){this.balance = balance;}

    public synchronized void add(Long money){
        balance += money;
        System.out.println("Operation : \"Put\"  put(amount) : " + money + "  new balance : " + balance);
    }
    public synchronized void withdraw(Long money){
        balance -= money;
        System.out.println("Operation : \"Withdraw\"  withdraw(amount) : " + money + "  new balance : " + balance);
    }
}
