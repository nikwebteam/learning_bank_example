
public class AddMoney extends Thread{

    public AddMoney(){
       start();
    }

    @Override
    public void run(){
        while (!Bank.isBankomatWorks) {
            try {
                Bank.account.add(1000L);
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
