
public class Bank {

    public static Account account = new Account(1000L);
    public static volatile boolean isBankomatWorks = false;

    public static void main(String[] args) throws InterruptedException {

        AddMoney add = new AddMoney();
        WithdrawMoney w1 = new WithdrawMoney();
        WithdrawMoney w2 = new WithdrawMoney();
        WithdrawMoney w3 = new WithdrawMoney();
        WithdrawMoney w4 = new WithdrawMoney();
        Thread.sleep(3000);
        isBankomatWorks = true;
    }
}
