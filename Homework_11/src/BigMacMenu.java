
public class BigMacMenu {
    private boolean isHamburgerReady = false;
    private boolean isFriesReady = false;
    private boolean isColaReady = false;
    private boolean isBigMacMenuReady = false;

    public boolean isHamburgerReady() {
        return isHamburgerReady;
    }

    public void setIsHamburgerReady(boolean isHamburgerReady) {
        this.isHamburgerReady = isHamburgerReady;
    }

    public boolean isFriesReady() {
        return isFriesReady;
    }

    public void setIsFriesReady(boolean isFriesReady) {
        this.isFriesReady = isFriesReady;
    }

    public boolean isColaReady() {
        return isColaReady;
    }

    public void setIsColaReady(boolean isColaReady) {
        this.isColaReady = isColaReady;
    }

    public boolean isBigMacMenuReady() {
        return isBigMacMenuReady;
    }

    public void setIsBigMacMenuReady(boolean isBigMacMenuReady) {
        this.isBigMacMenuReady = isBigMacMenuReady;
    }
}
