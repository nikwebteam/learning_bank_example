import javafx.scene.layout.Priority;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CarTotalizator {
    public static int luckyNumber;

    public static void main(String[] args) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        luckyNumber = Integer.parseInt(rd.readLine());

        List<Car> listCar = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            listCar.add(new Car(i));
        }
        for (Car c : listCar){
            c.start();
        }
    }

    static class Car extends Thread {
        Car(Integer number){
            super("Car with number " + number);
            if (number == luckyNumber){
                super.setPriority(Car.MAX_PRIORITY);
            }else {
                super.setPriority(Car.MIN_PRIORITY);
            }
        }

        public void run(){
            for (int i = 0; i < 10000; i++){
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Car with number " + getName() + " is finished!");
        }
    }
}
