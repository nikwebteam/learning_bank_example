public class Conveyor {

    public static void main(String[] args) throws InterruptedException {

        Worker vasya = new Worker("Vasya");
        Worker petya = new Worker("Petya");
        vasya.start();
        vasya.join();
        petya.start();
    }
    public static class Worker extends Thread{

        Worker(String name){
            super(name);
        }

        public void run(){
            System.out.println("Worker " + getName() + " started to work!");
            for (int i = 0; i < 1000; i++) {
                System.out.println("Detail number " + i + " is done by " + getName());
            }
        }
    }
}
