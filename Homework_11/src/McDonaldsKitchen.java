
public class McDonaldsKitchen {
    private static BigMacMenu bigMacMenu = new BigMacMenu();

    public static void main(String[] args) throws InterruptedException {
         McDonaldsKitchen.createBigMacMenu();
        synchronized (bigMacMenu){
            while (!bigMacMenu.isColaReady()){
                bigMacMenu.wait();
            }
            System.out.println("Big mac menu is ready!");
        }
    }
      //  ������� � ��������������
//    public static void createBigMacMenu(){
//        Thread hamburger = new Thread(() -> {
//            System.out.println("Hamburger cooking started.");
//            synchronized (bigMacMenu){
//                try {
//                    Thread.sleep(2000);
//                    bigMacMenu.setIsHamburgerReady(true);
//                    System.out.println("Hamburger ready!");
//                    bigMacMenu.notifyAll();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        Thread fries = new Thread(() -> {
//            synchronized (bigMacMenu) {
//                System.out.println("Fries cooking started.");
//                try {
//                    System.out.println("Waiting for hamburger...");
//                    while (!bigMacMenu.isHamburgerReady()) {
//                        bigMacMenu.wait();
//                    }
//                    Thread.sleep(2000);
//                    bigMacMenu.setIsFriesReady(true);
//                    System.out.println("Fries ready!");
//                    bigMacMenu.notifyAll();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        Thread cola  = new Thread(() -> {
//            synchronized (bigMacMenu) {
//                System.out.println("Cola cooking started.");
//                try {
//                    System.out.println("Waiting for fries...");
//                    while (!bigMacMenu.isFriesReady()) {
//                        bigMacMenu.wait();
//                    }
//                    Thread.sleep(2000);
//                    bigMacMenu.setIsColaReady(true);
//                    System.out.println("Cola ready!");
//                    bigMacMenu.notifyAll();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        cola.start();
//        hamburger.start();
//        fries.start();
//    }
    // ������� �� join
    public static void createBigMacMenu(){
        Thread hamburger = new Thread(() -> {
            System.out.println("Hamburger cooking started.");
            try {
                Thread.sleep(2000);
                System.out.println("Hamburger ready!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread fries = new Thread(() -> {
            System.out.println("Fries cooking started.");
            try {
                System.out.println("Waiting for hamburger...");
                hamburger.join();
                Thread.sleep(2000);
                System.out.println("Fries ready!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread cola  = new Thread(() -> {
            System.out.println("Cola cooking started.");
            try {
                System.out.println("Waiting for fries...");
                fries.join();
                Thread.sleep(2000);
                System.out.println("Cola ready!");
                bigMacMenu.setIsColaReady(true);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        cola.start();
        hamburger.start();
        fries.start();

    }
}
