
public class ParalellMultipleMatrix {

    public static void main(String[] args) {

        int[][] first = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12},{2,3,5,4}};
        int[][] second = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12},{2,3,5,4}};

        int[][] res = paralell(first,second);

        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < res[0].length; j++) {
                System.out.print(res[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[][] paralell(int[][] first, int[][] second){

        int[][] res = new int[first.length][second[0].length];

        for (int i = 0; i < first.length; ++i) {
            for (int j = 0; j < second[0].length; ++j) {
                final int I = i;
                final int J = j;
                Thread t = new Thread(() ->{
                    int li = I;
                    int lj = J;
                    for (int e = 0; e < first[0].length; ++e) {
                        res[li][lj] += first[li][e] * second[e][lj];
                    }
                });
                t.start();
            }
        }
        return res;
    }
}

