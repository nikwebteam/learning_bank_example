
public class WithdrawMoney extends Thread{

    WithdrawMoney(){
      start();
    }

    public void run(){
        while (!Bank.isBankomatWorks) {
            try {
                Bank.account.withdraw(100L);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
