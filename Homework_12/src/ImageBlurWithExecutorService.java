import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
/*
* Размытие изображения
* */
public class ImageBlurWithExecutorService {
    private static int imgSize;
    //Число пикселей в маске (13 - значит справа и слева по 6)
    private static int mBlurWidth = 13;

    public static void main(String[] args) {
        try {
            System.out.println("Loading image...");

            BufferedImage lImg = ImageIO.read(new File("cat.jpg"));
            System.out.println("Image loaded.");

            imgSize = lImg.getWidth();

            System.out.println("Processing image...");
            BufferedImage rImg = process(lImg);
            System.out.println("Image processed");

            System.out.println("Saving image...");
            ImageIO.write(rImg, "jpg", new File("out.jpg"));
            System.out.println("Image saved");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static BufferedImage process(BufferedImage lImg) throws ExecutionException, InterruptedException {
        //представляем изображение, как массив пикселей ()
        int[] rgb = imgToRgb(lImg);
        Thread.sleep(10000);
        long start = System.currentTimeMillis();
        //задаем число потоков и задач
        int[] transformed = blurParallel(rgb, 2, 100);
        long end = System.currentTimeMillis();

        System.out.println((end - start) + "ms");

        //переводим массив пикселей в изображение
        return rgbToImg(transformed);
    }


    private static int[] blurParallel(int[] rgb, int threadsCount, int tasksCount) throws ExecutionException, InterruptedException {
        //результирующий массив
        int[] res = new int[rgb.length];
        final int partSize = rgb.length / tasksCount;
        /*
        * здесь создаем ExecutorService, добавляем в него задачи, которые будут заполнять результирующий массив
        * т.е каждая задача берет на себя расчет определенного количества пикселей(partSize) результирующего массива
        * расчет значений пикселей выполнять в методе computeDirectly
        * ждем пока все задачи отработают и закрываем ExecutorService
        * */
        ExecutorService exe = Executors.newFixedThreadPool(threadsCount);
        List<Future<?>> futureList = new ArrayList<>();
        for (int i = 0; i < tasksCount; i++) {
            final int finalI = i;
            futureList.add(exe.submit(()->{
                int from = finalI * partSize;
                computeDirectly(rgb, from, from + partSize, res);
            }));
        }
        for (Future<?> p : futureList){
            p.get();
        }
        exe.shutdown();
        return res;
    }

    static protected void computeDirectly(int[] source, int start, int length, int[] destination) {
        /*
        * расчет пикселей начиная с индекса start до start + length
        * тут реализовать алгоритм расчета среднего значения пикселя, исходя из ширины маски (это mBlurWidth)
        * после расчета очередного пикселя, помещаем его в массив destination
        * */
        int[] result = new int[mBlurWidth];
        for (int i = start; i < length; i++) {
            result = getPixel(i, source);
            destination[i] = mergeRGB(result);
        }
    }

    private static int[] imgToRgb(BufferedImage img) {
        int[] res = new int[imgSize * imgSize];
        img.getRGB(0, 0, imgSize, imgSize, res, 0, imgSize);
        return res;
    }

    private static BufferedImage rgbToImg(int[] rgb) {
        BufferedImage res = new BufferedImage(imgSize, imgSize, BufferedImage.TYPE_INT_RGB);
        res.setRGB(0, 0, imgSize, imgSize, rgb, 0, imgSize);
        return res;
    }

    private static int[] getRgbFromInt(int rgb){
        if (rgb != 0) {
            int[] result = new int[3];
            result[0] = (rgb>>16)&0xFF;
            result[1] = (rgb>>8)&0xFF;
            result[2] = (rgb)&0xFF;
            return result;
        }else{
            int[] result2 = new int[]{0,0,0};
            return result2;
        }
    }
    private static int mergeRGB(int[] list){
        int sumR = 0, sumG = 0, sumB = 0;

            for (int i = 0; i < list.length; i++) {
                int[] tmp = getRgbFromInt(list[i]);
                if (tmp[0] != 0) {
                    sumR += tmp[0];
                    sumG += tmp[1];
                    sumB += tmp[2];
                }
            }
        int result = setRgbToInt(sumR / list.length, sumG / list.length, sumB / list.length);
        return result;
    }

    public static int setRgbToInt(int r, int g, int b){
        int res = (0xff000000) | (r<<16) | (g<<8)| (b);
        return res;
    }

    public static int[] getPixel(int current, int[] source ){
        if (mBlurWidth < 2){mBlurWidth = 3;}
        int[] result = new int[mBlurWidth];
        int len;
        if (mBlurWidth > 2){
            if (mBlurWidth % 2 != 0) {
                len = (mBlurWidth - 1) / 2;
            }else{
                len = mBlurWidth / 2;
            }
        }else{
            len = 1;
        }
        if ((current + len < source.length) && (current - len >= 0)) {
            for (int i = current + len, j = 0; i > current - len; i--, j++) {
                result[j] = source[i];
            }
        }else if (current - len < 0){
            int tmp = current - len;
            for (int i = tmp, j = 0; i < current + len; i++, j++) {
                if (i < 0){
                    result[j] = 0;
                }else{
                    result[j] = source[i];
                }
            }
        }else if (current + len > source.length){
            for (int i = current + len, j = 0; i >= current - len ; i--, j++) {
                if (i >= source.length){
                    result[j] = 0;
                }else{
                    result[j] = source[i];
                }
            }
        }
        return result;
    }
}
