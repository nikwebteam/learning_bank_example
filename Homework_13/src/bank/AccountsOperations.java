package bank;

import java.math.BigDecimal;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class AccountsOperations {

    private static void printAccount(ResultSet result) throws SQLException {
        String fn, ln, bd, ad, ct;
        fn = result.getString("accountnumber");
        ln = result.getString("balance");
        bd = result.getTimestamp("creationdate").toString();
        ad = result.getString("currency");
        ct = result.getString("blocked");
        System.out.println(fn + " " + ln + " " + bd + " " + ad + " " + ct);
    }
    public static void selectAccounts() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from accounts");
            while (resultSet.next()) {
                printAccount(resultSet);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            resultSet.close();
            statement.close();
            connection.close();
        }
    }
    public static List<Long> selectAccountsId(Connection connection) throws SQLException {
        List<Long> result = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select accountnumber from accounts");
            while (!resultSet.isLast()) {
                resultSet.next();
                result.add(resultSet.getLong("accountnumber"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            resultSet.close();
            statement.close();
        }
        return result;
    }
    public static void smartInsert(Double balance,String creationDate,String currency,boolean blocked,Long userId) throws SQLException, ParseException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.prepareStatement("insert into accounts (balance, creationdate, currency, blocked, customerid)" +
                    "values(?, ?, ?, ?, ?)");
            statement.setBigDecimal(1, BigDecimal.valueOf(balance));
            statement.setTimestamp(2, java.sql.Timestamp.valueOf(creationDate));
            statement.setString(3, currency);
            statement.setBoolean(4, blocked);
            statement.setLong(5, userId);
            statement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            statement.close();
            connection.close();
        }
    }
    public static void batchAccountsInsert() throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.prepareStatement("insert into accounts (balance, creationdate, currency, blocked, customerid)" +
                    "values(0.0, ?, 'UAN', false, ?)");
            List<Long> longs = CustomersOperations.selectCustomersId(connection);
            for (Long p : longs){
                statement.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
                statement.setLong(2, p);
                statement.addBatch();
            }
            statement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            statement.close();
            connection.close();
        }
    }
}
