package bank;

import org.postgresql.Driver;

import java.sql.*;

public class ConnectionCreater {

    private static Connection connection = null;

    public static Connection create() throws SQLException {
        if (connection == null || connection.isClosed()) {
            try {
                Driver driver = new org.postgresql.Driver();
                DriverManager.registerDriver(driver);
                connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/bank", "postgres", "60ajlokin");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return connection;
        }
        return connection;
    }
}
