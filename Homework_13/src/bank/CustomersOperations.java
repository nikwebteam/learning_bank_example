package bank;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomersOperations {

    private static void printCustomer(ResultSet result) throws SQLException {
        String fn, ln, bd, ad, ct, ps, ph;
        fn = result.getString("firstname");
        ln = result.getString("lastname");
        bd = result.getDate("birthdate").toString();
        ad = result.getString("address");
        ct = result.getString("city");
        ps = result.getString("passport");
        ph = result.getString("phone");
        System.out.println(fn + " " + ln + " " + bd + " " + ad + " " + ct + " " + ps + " " + ph);
    }
    public static void selectAllCustomers() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM customers");
            while (resultSet.next()) {
                printCustomer(resultSet);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            resultSet.close();
            statement.close();
            connection.close();
        }
    }
    public static List<Long> selectCustomersId(Connection connection) throws SQLException {
        List<Long> result = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select id from customers");
            while (resultSet.next()) {
                result.add(resultSet.getLong("id"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            resultSet.close();
            statement.close();
        }
        return result;
    }
    public static void selectCustomersByPassport(String passport) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.prepareStatement("select * from customers where passport = ?");
            statement.setString(1, passport);
            resultSet = statement.executeQuery();
            if (resultSet.next()){
                printCustomer(resultSet);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            resultSet.close();
            statement.close();
            connection.close();
        }
    }
    public static void constantInsert() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.createStatement();
            statement.execute("insert into customers (firstname, lastname, birthdate, address, city, passport, phone)" +
                    "values('testfirstname', 'testlastname', '2012-12-12', 'shkolnaya street 45', 'dnepropetrovsk', 'am995658', 0506554789)");
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            statement.close();
            connection.close();
        }
    }
    public static void smartInsert(String firstName,String lastName,String birthdate,String address,String city,String passport,String phone) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.prepareStatement("insert into customers (firstname, lastname, birthdate, address, city, passport, phone)" +
                    "values(?, ?, ?, ?, ?, ?, ?)");
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setDate(3, java.sql.Date.valueOf(birthdate));
            statement.setString(4, address);
            statement.setString(5, city);
            statement.setString(6, passport);
            statement.setString(7, phone);
            statement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            statement.close();
            connection.close();
        }
    }
    public static void updateCustomerFirstAndLastName(Long id, String firstName, String lastName) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.prepareStatement("update customers set firstname = ?, lastname = ? where id = ?");
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setLong(3, id);
            statement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            statement.close();
            connection.close();
        }
    }
    public static void deleteCustomer(Long id) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.prepareStatement("DELETE FROM customers WHERE id = ?");
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            statement.close();
            connection.close();
        }
    }
}
