package bank;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TransactionsOperations {

    public static void batchTransactionsInsert() throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.prepareStatement("INSERT INTO transactions(amount, date, operationtype, accountnumber) VALUES (100.00, ?, 'PUT', ?)");
            List<Long> longs = AccountsOperations.selectAccountsId(connection);
            for (Long p : longs){
                statement.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
                statement.setLong(2, p);
                statement.addBatch();
            }
            statement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            statement.close();
            connection.close();
        }

    }
    public static void updateAllTransactions() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            resultSet = statement.executeQuery("select * from transactions");
            while (resultSet.next()){
                if (resultSet.getString("operationtype").equalsIgnoreCase("put")) {
                    resultSet.updateBigDecimal("amount", new BigDecimal(150));
                    resultSet.updateRow();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            resultSet.close();
            statement.close();
            connection.close();
        }
    }
    public static void deleteAllWithdraw() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionCreater.create();
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            resultSet = statement.executeQuery("SELECT * from transactions");
            List<Long> accounts = new ArrayList<>();
            while (resultSet.next()){
                if (resultSet.getString("operationtype").equalsIgnoreCase("WSD")){
                    accounts.add(resultSet.getLong("accountnumber"));
                    resultSet.deleteRow();
                }
            }
            ps = connection.prepareStatement("insert into transactions(amount, date, operationtype, accountnumber)" +
                    " values (100.00, ?, 'PUT', ?)");
            for (Long p : accounts){
                ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
                ps.setLong(2, p);
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            resultSet.close();
            statement.close();
            ps.close();
            connection.close();
        }
    }
}
