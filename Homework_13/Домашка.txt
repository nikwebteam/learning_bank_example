1) select name from city where countrycode = 'UKR';

2) select count(name) from city where countrycode = 'UKR';

3) select name from city where countrycode = 'UKR' and population = (select max(population) from city where countrycode = 'UKR');

4) select district, count(name) from city where countrycode = 'UKR' group by district;

5) select district, sum(population) from city where countrycode = 'UKR' group by district;

6) select district, sum(population) as r from city where countrycode = 'UKR' group by district order by r desc;

7) select name from country where continent = 'Europe';

8) select c.name, ct.name  from country as c join city as ct on  c.capital=ct.id where continent = 'Europe';

9) select region, sum(surfacearea) from country where continent = 'Europe' group by region;

10) select name, indepyear from country where indepyear between 55 and 95 order by indepyear asc;

11) select name from country where governmentform like '%Monarchy%';

12) select name from country where surfacearea = (select max(surfacearea) from country);

13) select region, lifeexpectancy from country;

14) select continent, avg(lifeexpectancy) from country group by continent;

15) select name from country join countrylanguage on country.code=countrylanguage.countrycode
where language = 'Ukrainian';

16) select name from country join countrylanguage on country.code=countrylanguage.countrycode
where language = 'English' and isofficial = true;

17) select cn.name, ct.name, cl.language, cl.isofficial  from country as cn join city as ct on cn.capital=ct.id
join countrylanguage as cl on cl.countrycode=cn.code where isofficial = true;

18) select cn.name, ct.name, count(cl.language) as officiallanguage
from country as cn join city as ct on cn.capital=ct.id
join countrylanguage as cl on cl.countrycode=cn.code
where isofficial = true
group by cn.name,ct.name
order by officiallanguage desc;
