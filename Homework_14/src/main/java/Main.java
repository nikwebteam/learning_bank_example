import com.company.service.AccountService;
import com.company.service.CustomerService;
import com.company.service.TransactionService;

public class Main {

    private static CustomerService customerService = new CustomerService();
    private static AccountService accountService = new AccountService();
    private static TransactionService transactionService = new TransactionService();

    public static void main(String[] args) {
        customerService.printCustomers();
        System.out.println();
        accountService.printAccounts();
        System.out.println();
        transactionService.printTransaction();
    }
}
