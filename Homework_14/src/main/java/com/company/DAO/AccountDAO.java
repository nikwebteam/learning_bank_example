package com.company.DAO;

import com.company.Util.ConnectionUtil;
import com.company.domain.Account;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountDAO implements IAccountDAO {


    @Override
    public Account get(Long accountNumber){
        String sql = "SELECT * FROM accounts WHERE accountnumber = ?";
        try(Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setLong(1, accountNumber);
            return extractAccount(statement.executeQuery());
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long save(Account account) {
        String sql = "INSERT INTO accounts (balance, creationDate, currency, blocked, customerId)" +
                "VALUES (?, ?, ?, ?, ?)";
        try (Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setBigDecimal(1, account.getBalance());
            statement.setTimestamp(2, account.getCreationDate());
            statement.setString(3, account.getCurrency());
            statement.setBoolean(4, account.getBlocked());
            statement.setLong(5, account.getCustomerId());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Account account) {
        String sql = "UPDATE accounts SET" +
                "balance = ?," +
                "blocked = ? WHERE accountnumber = ?";
        try (Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setBigDecimal(1, account.getBalance());
            statement.setBoolean(2, account.getBlocked());
            statement.setLong(3, account.getId());
            statement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Account account) {
        String sql = "DELETE FROM accounts WHERE accountnumber = ?";
        try (Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setLong(1, account.getId());
            statement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void deleteByCustomer(Long customerId) {
        String sql = "DELETE FROM accounts WHERE customerid = ?";
        try(Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setLong(1, customerId);
            statement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public List<Account> list() {
        List<Account> list = new ArrayList<>();
        String sql = "SELECT * FROM accounts";
        try (Connection connection = ConnectionUtil.create()){
            Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                list.add(extractAccount(resultSet));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return list;
    }

    private Account extractAccount(ResultSet resultSet) throws SQLException {
        Account account = new Account(
                resultSet.getBigDecimal("balance"),
                resultSet.getTimestamp("creationDate"),
                resultSet.getString("currency"),
                resultSet.getBoolean("blocked"),
                resultSet.getLong("customerId")
        );
        account.setId(resultSet.getLong("accountnumber"));
        return account;
    }
}
