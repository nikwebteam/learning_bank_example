package com.company.DAO;

import com.company.Util.ConnectionUtil;
import com.company.domain.Account;
import com.company.domain.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAO implements ICustomerDAO{

    @Override
    public Customer get(Long accountNumber) {
        String sql = "SELECT c.firstname, c.lastname, c.birthdate, c.address, c.city, c.passport, c.phone" +
                     "FROM customers as c join accounts as a on c.id = a.customerid" +
                     "WHERE a.accountnumber = ?";
        try(Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setLong(1, accountNumber);
            return extractCustomer(statement.executeQuery());
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long save(Customer customer) {
        String sql = "INSERT INTO customers (" +
                "firstName," +
                "lastName," +
                "birthDate," +
                "address," +
                "city," +
                "passport," +
                "phone,) VALUES (?,?,?,?,?,?,?)";
        try(Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setDate(3, customer.getBirthDate());
            statement.setString(4, customer.getAddress());
            statement.setString(5, customer.getCity());
            statement.setString(6, customer.getPassport());
            statement.setString(7, customer.getPhone());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Customer customer) {
        String sql = "UPDATE customers" +
                "SET firstName = ?," +
                "lastname = ?," +
                "birthDate = ?," +
                "address = ?," +
                "city = ?," +
                "passport = ?," +
                "phone = ? WHERE id = ?";
        try(Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setDate(3, customer.getBirthDate());
            statement.setString(4, customer.getAddress());
            statement.setString(5, customer.getCity());
            statement.setString(6, customer.getPassport());
            statement.setString(7, customer.getPhone());
            statement.setLong(8, customer.getId());
            statement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Customer customer) {
        String sql = "DELETE FROM customers WHERE id = ?";
        try(Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setLong(1, customer.getId());
            statement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public List<Customer> customersList() {
        String sql = "SELECT * FROM customers";
        List<Customer> list = new ArrayList<>();
        try(Connection connection = ConnectionUtil.create()){
            Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                list.add(extractCustomer(resultSet));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<Account> accountList(Customer customer) {
        List<Account> list = new ArrayList<>();
        String sql = "SELECT * FROM accounts WHERE customerid = ?";
        try(Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                list.add(extractAccount(resultSet));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return list;
    }

    private Account extractAccount(ResultSet resultSet) throws SQLException {
        Account account = new Account(
                resultSet.getBigDecimal("balance"),
                resultSet.getTimestamp("creationDate"),
                resultSet.getString("currency"),
                resultSet.getBoolean("blocked"),
                resultSet.getLong("customerId")
        );
        account.setId(resultSet.getLong("id"));
        return account;
    }

    private Customer extractCustomer(ResultSet resultSet) throws SQLException {
        return new Customer(
                resultSet.getString("firstName"),
                resultSet.getString("lastName"),
                resultSet.getDate("birthDate"),
                resultSet.getString("address"),
                resultSet.getString("city"),
                resultSet.getString("passport"),
                resultSet.getString("phone")
        );
    }

}
