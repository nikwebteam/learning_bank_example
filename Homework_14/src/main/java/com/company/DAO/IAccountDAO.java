package com.company.DAO;


import com.company.domain.Account;

import java.util.List;

public interface IAccountDAO {

    Account get(Long accountNumber);
    Long save(Account account);
    void update(Account account);
    void delete(Account account);
    void deleteByCustomer(Long customerId);
    List<Account> list();
}
