package com.company.DAO;

import com.company.domain.Account;
import com.company.domain.Customer;

import java.util.List;

public interface ICustomerDAO {

    Customer get(Long accountNumber);
    Long save(Customer account);
    void update(Customer account);
    void delete(Customer account);
    List<Customer> customersList();
    List<Account> accountList(Customer customer);
}
