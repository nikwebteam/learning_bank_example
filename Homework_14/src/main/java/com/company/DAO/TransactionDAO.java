package com.company.DAO;

import com.company.Util.ConnectionUtil;
import com.company.domain.Account;
import com.company.domain.Transaction;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TransactionDAO implements ITransactionDAO{

    @Override
    public Transaction get(Long accountNumber) {
        String sql = "SELECT * FROM transactions WHERE accountnumber = ?";
        try (Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            statement.setLong(1, accountNumber);
            return extractTransaction(statement.executeQuery());
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void delete(Long accountId) {
        String sql = "DELETE FROM transactions WHERE accountnumber = ?";
        try (Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE , ResultSet.CONCUR_UPDATABLE);
            statement.setLong(1, accountId);
            statement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {

        }
    }

    @Override
    public Long save(Transaction transaction) {
        String sql = "INSERT INTO transactions (amount, date, operationType, accountNumber)" +
                "VALUES (?, ?, ?, ?)";
        try(Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setBigDecimal(1, transaction.getAmount());
            statement.setTimestamp(2, transaction.getDate());
            statement.setString(3, transaction.getOperationType());
            statement.setLong(4, transaction.getAccountNumber());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Transaction transaction) {
        String sql = "UPDATE transactions SET" +
                "amount = ?," +
                "date = ?," +
                "operationType = ?," +
                "accountNumber = ? WHERE id = ?";
        try (Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setBigDecimal(1, transaction.getAmount());
            statement.setTimestamp(2, transaction.getDate());
            statement.setString(3, transaction.getOperationType());
            statement.setLong(4, transaction.getAccountNumber());
            statement.setLong(5, transaction.getId());
            statement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Transaction transaction) {
        String sql = "DELETE FROM transactions WHERE id = ?";
        try (Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setLong(1, transaction.getId());
            statement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public List<Transaction> list() {
        List<Transaction> list = new ArrayList<>();
        String sql = "SELECT * FROM transactions";
        try (Connection connection = ConnectionUtil.create()){
            Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                list.add(extractTransaction(resultSet));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<Transaction> list(Account account) {
        List<Transaction> list = new ArrayList<>();
        String sql = "SELECT * FROM transactions WHERE accountnumber = ?";
        try (Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                list.add(extractTransaction(resultSet));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return list;
    }

    private Transaction extractTransaction(ResultSet resultSet) throws SQLException {
        return new Transaction(
                resultSet.getBigDecimal("amount"),
                resultSet.getTimestamp("date"),
                resultSet.getString("operationType"),
                resultSet.getLong("accountNumber")
        );
    }
}
