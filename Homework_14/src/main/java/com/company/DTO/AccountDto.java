package com.company.DTO;

import com.company.domain.Account;
import com.company.domain.Transaction;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class AccountDto {

    private BigDecimal balance;
    private Timestamp creationDate;
    private String currency;
    private Boolean blocked;
    private Long customerId;
    private List<Transaction> transactions;

    public AccountDto(Account account, List<Transaction> transactions){
        balance = account.getBalance();
        creationDate = account.getCreationDate();
        currency = account.getCurrency();
        blocked = account.getBlocked();
        customerId = account.getCustomerId();
        this.transactions = transactions;
    }
}
