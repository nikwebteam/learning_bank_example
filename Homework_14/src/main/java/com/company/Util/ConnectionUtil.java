package com.company.Util;


import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

    private static Connection connection;

    public static Connection create() throws SQLException {
        if (connection == null || connection.isClosed()) {
            Driver driver = new org.postgresql.Driver();
            try {
                Class.forName("org.postgresql.Driver");
                return DriverManager.getConnection("jdbc:postgresql://localhost:5432/bank", "postgres", "60ajlokin");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return  connection;
    }
}
