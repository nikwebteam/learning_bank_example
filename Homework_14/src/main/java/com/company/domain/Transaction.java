package com.company.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Transaction {

    private Long id;
    private BigDecimal amount;
    private Timestamp date;
    private String operationType;
    private Long accountNumber;

    public Transaction(BigDecimal amount, Timestamp date, String operationType, Long accountNumber) {
        this.amount = amount;
        this.date = date;
        this.operationType = operationType;
        this.accountNumber = accountNumber;
    }

    public Transaction(BigDecimal amount, String operationType, Long accountNumber){
        this(amount, new Timestamp(System.currentTimeMillis()), operationType, accountNumber);
    }

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "amount=" + amount +
                ", date=" + date +
                ", operationType='" + operationType + '\'' +
                ", accountNumber=" + accountNumber +
                '}';
    }
}
