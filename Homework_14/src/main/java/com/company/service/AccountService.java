package com.company.service;

import com.company.DAO.AccountDAO;
import com.company.DAO.IAccountDAO;
import com.company.domain.Account;
import com.company.domain.Customer;
import com.company.domain.Transaction;

import java.math.BigDecimal;

public class AccountService {

    private IAccountDAO accountDAO = new AccountDAO();
    private TransactionService transactionService = new TransactionService();

    public void save(Account account){
        Long id = accountDAO.save(account);
        if (!account.getBalance().equals(BigDecimal.ZERO)){
            transactionService.save(new Transaction(account.getBalance(), "PUT", id));
        }
    }

    public void printAccounts(){
        for (Account account : accountDAO.list()){
            System.out.println(account);
        }
    }

    public void delete(Account account){
        transactionService.delete(account);
        accountDAO.delete(account);
    }

    public void deleteAll(Customer customer){
        for (Account account : accountDAO.list()){
            delete(account);
        }
        accountDAO.deleteByCustomer(customer.getId());
    }
}
