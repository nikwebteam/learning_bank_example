package com.company.service;


import com.company.DAO.CustomerDAO;
import com.company.DAO.ICustomerDAO;
import com.company.domain.Account;
import com.company.domain.Customer;

public class CustomerService {

    private ICustomerDAO customerDAO = new CustomerDAO();
    private AccountService accountService = new AccountService();

    public void save(Customer customer){
        Long id = customerDAO.save(customer);
        if (id != null) {
            accountService.save(new Account(id, "UAN"));
        }
    }

    public void printCustomers(){
        for (Customer customer : customerDAO.customersList()){
            System.out.println(customer);
        }
    }

    public void delete(Customer customer){
        accountService.deleteAll(customer);
        customerDAO.delete(customer);
    }

}
