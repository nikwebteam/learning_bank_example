package com.company.service;

import com.company.DAO.ITransactionDAO;
import com.company.DAO.TransactionDAO;
import com.company.domain.Account;
import com.company.domain.Transaction;

public class TransactionService {

    private ITransactionDAO transactionDAO = new TransactionDAO();

    public void save(Transaction transaction){
        transactionDAO.save(transaction);
    }

    public void printTransaction(){
        for (Transaction transaction : transactionDAO.list()){
            System.out.println(transaction);
        }
    }

    public void delete(Transaction transaction){
        transactionDAO.delete(transaction);
    }

    public void delete(Account account){
        transactionDAO.delete(account.getId());
    }
}
