
import com.company.domain.Customer;
import com.company.service.AccountService;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.sql.Date;

public class AccountServiceTest {

    private AccountService accountService;
    private Customer customer;

    @BeforeTest
    public void instance(){
        accountService = new AccountService();
        customer = new Customer("Nik", "Chupikov", new Date(System.currentTimeMillis()), "Ukraine", "Dnepr", "AN677432", "380956774156");
    }

    @AfterClass
    public void cleanDB(){

    }

    @Test
    public void saveAccountTest(){

    }

    @Test
    public void deleteAccountTest(){

    }

    @Test
    public void deleteAllAccountTest(){

    }
}
