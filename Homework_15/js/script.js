function openTable() {
    document.getElementsByClassName("add")[0].style.display = 'block';
    document.getElementsByClassName("create")[1].style.display = 'none';

	var inputs = document.querySelectorAll(".add input");

	for (var i = 0; i < inputs.length; i++) {
		inputs[i].addEventListener("blur", function(event){
			var tmp = event.currentTarget.nextSibling;
			var next = tmp.nextSibling;
			if(event.currentTarget.value == ''){
				next.style.display = 'block';
			}else{
				if(next.style.display == 'block'){
					next.style.display = 'none';
				}
			}
		});
	}
}

function hideTable() {
	var spans = document.querySelectorAll(".required");
	var inputs = document.querySelectorAll(".add input");
	var monitor = true;
	for (var i = 0; i < inputs.length; i++) {
		if(inputs[i].value == ''){
			monitor = false;
			spans[i].style.display = 'block';
		}
	}
	if(monitor){
		var firstName = document.getElementById("firstName").value;
		var lastName = document.getElementById("lastName").value;
		var birthDate = document.getElementById("birthDate").value;
		var address = document.getElementById("address").value;
		var city = document.getElementById("city").value;
		var passport = document.getElementById("passport").value;
		var phone = document.getElementById("phone").value;
		var cutomerPage = "customer.html"

		var str = "<tr>" + "<td>" + "<a href=\"" + cutomerPage + "\">" + "<span>" + firstName + "</span>" + "</a>" + "</td>" +
						 "<td>" + "<span>" + lastName + "</span>" + "</td>" +
						 "<td>" + "<span>" + birthDate + "</span>" + "</td>" +
						 "<td>" + "<span>" + address + "</span>" + "</td>" +
						 "<td>" + "<span>" + city + "</span>" + "</td>" +
						 "<td>" + "<span>" + passport + "</span>" + "</td>" +
						 "<td>" + "<span>" + phone + "</span>" + "</td>" +
						 "<td><span class=\"delete\"><a href=\"#\"><img src=\"../images/delete.png\"></a></span></td></tr>";

		var bod = document.getElementsByTagName("tbody")[0];
		bod.innerHTML += str;
		document.getElementsByClassName("add")[0].style.display = 'none';
		document.getElementsByClassName("add")[0].reset();
        document.getElementsByClassName("create")[1].style.display = 'block';
        deleteRow();
	}
}

function deleteRow(){

	var rows = document.querySelectorAll(".delete > a");

	for (var i = 0; i < rows.length; i++) {
		rows[i].addEventListener("click", function(event){
			var elem = event.currentTarget;
			elem = elem.parentNode.parentNode.parentNode;
			elem.remove();
		});
	}

}

window.onload = function(){deleteRow();};