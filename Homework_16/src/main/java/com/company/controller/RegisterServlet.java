package com.company.controller;


import com.company.domain.Customer;
import com.company.domain.User;
import com.company.service.CustomerService;
import com.company.service.UserService;
import com.company.util.MD5Security;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

public class RegisterServlet extends HttpServlet{

    private UserService userService = new UserService();
    private CustomerService customerService = new CustomerService();
    private Pattern pattern = Pattern.compile("/registration");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        if (pattern.matcher(req.getRequestURI()).matches()){
            if (session.getAttribute("auth") != null){
                resp.sendRedirect("/");
                return;
            }
            req.getRequestDispatcher("/WEB-INF/jsp/registration.jsp").forward(req, resp);
        }
        else{
            resp.sendError(404);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        if (req.getParameterMap().size() == 1 && req.getParameter("login") != null){
            if (userService.get(MD5Security.md5(req.getParameter("login"))) != null){
                resp.getWriter().write("false");
                return;
            }else{
                resp.getWriter().write("true");
                return;
            }
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date parsed = null;
        try {
            parsed = format.parse(req.getParameter("birthDate"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Customer customer = new Customer(
                req.getParameter("firstName"),
                req.getParameter("lastName"),
                new java.sql.Date(parsed.getTime()),
                req.getParameter("address"),
                req.getParameter("city"),
                req.getParameter("passport"),
                req.getParameter("phone")
        );
        Long customerId = customerService.save(customer);
        customer.setId(customerId);
        String salt = MD5Security.createSalt();
        String role = req.getParameter("role") != null ? req.getParameter("role"):"USER";
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        User user = new User(
                MD5Security.md5(login),
                MD5Security.md5(password + salt),
                salt,
                role,
                customerId
        );
        userService.save(user);
        if (session.getAttribute("auth") == null) {
            session.setAttribute("auth", MD5Security.md5(login));
            resp.sendRedirect("/");
        }else{
            req.setAttribute("customer", customer);
            req.getRequestDispatcher("/WEB-INF/jsp/rowBilder/customerRow.jsp").forward(req, resp);
        }
    }

}
