package com.company.controller;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        List<String> patterns = Arrays.asList("/customers/[0-9]+","/accounts/[0-9]+","/transactions/[0-9]+",
                "/accounts/create","/transactions/create",
                "/accounts/delete","/transactions/delete");

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();

        if (session.getAttribute("auth") == null){
             response.sendRedirect("/login");
        }
        else if (request.getSession().getAttribute("role").equals("ADMIN")){
            filterChain.doFilter(servletRequest, servletResponse);
        }
        else if (request.getSession().getAttribute("role").equals("USER")){
            String uri = request.getRequestURI();
            for (String pat : patterns){
                Pattern pattern = Pattern.compile(pat);
                if (pattern.matcher(uri).matches()){
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                }
            }
            response.sendRedirect("/customers/" + request.getSession().getAttribute("customerId"));
        }
    }

    @Override
    public void destroy() {}
}
