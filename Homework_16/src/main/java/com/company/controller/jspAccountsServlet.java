package com.company.controller;

import com.company.domain.Account;
import com.company.service.AccountService;
import com.company.service.TransactionService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.regex.Pattern;

public class jspAccountsServlet extends HttpServlet{

    private AccountService accountService = new AccountService();
    private TransactionService transactionService = new TransactionService();
    private Pattern accountPattern = Pattern.compile("/accounts/[0-9]+");
    private Pattern patternAccounts = Pattern.compile("/accounts");
    private Pattern patternDelete = Pattern.compile("/accounts/delete");
    private Pattern patternCreate = Pattern.compile("/accounts/create");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String URI = req.getRequestURI();

        if (patternAccounts.matcher(URI).matches()){
            req.setAttribute("accounts",accountService.getAllAccounts());
            req.getRequestDispatcher("/WEB-INF/jsp/accounts.jsp").forward(req, resp);
        }
        else if(accountPattern.matcher(URI).matches()){
            Long accountNumber = Long.parseLong(URI.substring(URI.lastIndexOf("/") + 1));
            Account account = accountService.get(accountNumber);
            if (account.getCustomerId().equals(req.getSession().getAttribute("customerId"))
                     || req.getSession().getAttribute("role").equals("ADMIN")){
                req.setAttribute("transactions", transactionService.getAllTransactions(accountNumber));
                req.setAttribute("customerId", account.getCustomerId());
                req.setAttribute("accountNumber", accountNumber);
                req.getRequestDispatcher("/WEB-INF/jsp/account.jsp").forward(req, resp);
            }
            else {
                resp.sendError(404);
            }
        }
        else {
            resp.sendError(404);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String URI = req.getRequestURI();

        if (patternCreate.matcher(URI).matches()){
            Account account = accountService.save(new Account(
                    Long.parseLong(req.getParameter("customerId")),
                    req.getParameter("currencyType"),
                    new BigDecimal(0)
            ));
            req.setAttribute("account", account);
            req.getRequestDispatcher("/WEB-INF/jsp/rowBilder/accountRow.jsp").forward(req, resp);
        }
        else if (patternDelete.matcher(URI).matches()){
            Long accountId = Long.parseLong(req.getParameter("id").trim());
            boolean monitor = false;
            for (Account account : accountService.getAllAccounts()){
                if (account.getId().equals(accountId)){
                    accountService.delete(account);
                    monitor = true;
                    break;
                }
            }
            if (monitor){
                resp.getWriter().write("success");
            }
            else {
                resp.sendError(500);
            }
        }
        else{
            resp.sendError(404);
        }
    }
}
