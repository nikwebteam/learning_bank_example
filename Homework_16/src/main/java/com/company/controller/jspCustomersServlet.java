package com.company.controller;

import com.company.domain.Customer;
import com.company.service.AccountService;
import com.company.service.CustomerService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

public class jspCustomersServlet extends HttpServlet{

    private CustomerService customerService = new CustomerService();
    private AccountService accountService = new AccountService();
    private Pattern customerPattern = Pattern.compile("/customers/[0-9]+");
    private Pattern patternCustomers = Pattern.compile("/customers");
    private Pattern patternDelete = Pattern.compile("/customers/delete");
    private Pattern patternCreate = Pattern.compile("/customers/create");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String URI = req.getRequestURI();

        if (patternCustomers.matcher(URI).matches()){
            req.setAttribute("customers",customerService.getAllCustomers());
            req.getRequestDispatcher("/WEB-INF/jsp/customers.jsp").forward(req, resp);
        }
        else if(customerPattern.matcher(URI).matches()){

            Long customerId = Long.parseLong(URI.substring(URI.lastIndexOf("/") + 1));
            if (customerId.equals(req.getSession().getAttribute("customerId"))
                    || req.getSession().getAttribute("role").equals("ADMIN")){
                Customer customer = customerService.getById(customerId);
                req.setAttribute("customerId", customerId);
                req.setAttribute("accounts", accountService.getAllAccounts(customerId));
                req.setAttribute("fullName", customer.getFirstName() + " " + customer.getLastName());
                req.getRequestDispatcher("/WEB-INF/jsp/customer.jsp").forward(req, resp);
            }else {
                resp.sendError(404);
            }
        }
        else {
            resp.sendError(404);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String URI = req.getRequestURI();

        if (patternCreate.matcher(URI).matches()){
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            java.util.Date parsed = null;
            try {
                parsed = format.parse(req.getParameter("birthdate"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Customer customer = new Customer(
                    req.getParameter("firstname"),
                    req.getParameter("lastname"),
                    new java.sql.Date(parsed.getTime()),
                    req.getParameter("address"),
                    req.getParameter("city"),
                    req.getParameter("passport"),
                    req.getParameter("phone")
            );
            Long id = customerService.save(customer);
            customer.setId(id);
            req.setAttribute("customer", customer);
            req.getRequestDispatcher("/WEB-INF/jsp/rowBilder/customerRow.jsp").forward(req, resp);
        }
        else if (patternDelete.matcher(URI).matches()){
            Long customerId = Long.parseLong(req.getParameter("id").trim());
            boolean monitor = false;
            for (Customer customer : customerService.getAllCustomers()){
                if(customer.getId().equals(customerId)){
                    customerService.delete(customer);
                    monitor = true;
                    break;
                }
            }
            if (monitor){
                resp.getWriter().write("success");
            }
            else {
                resp.sendError(500);
            }
        }
        else{
            resp.sendError(404);
        }
    }
}
