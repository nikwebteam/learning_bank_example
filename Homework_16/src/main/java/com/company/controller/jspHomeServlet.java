package com.company.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class jspHomeServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String URI = req.getRequestURI();
        if (URI.equals("/")) {
           req.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(req,resp);
        }else {
            resp.sendError(404);
        }
    }
}
