package com.company.controller;

import com.company.domain.User;
import com.company.service.UserService;
import com.company.util.MD5Security;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.regex.Pattern;

public class jspLoginServlet extends HttpServlet{

    private UserService userService = new UserService();
    private Pattern patternLogin = Pattern.compile("/login");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String URI = req.getRequestURI();

        if (patternLogin.matcher(URI).matches()){
            HttpSession session = req.getSession();
            if (session.getAttribute("auth") == null) {
                req.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, resp);
            }else{
                resp.sendRedirect("/");
            }
        }
        else {
            resp.sendError(404);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String password = req.getParameter("password");
        String login = req.getParameter("login");
        User user = userService.get(MD5Security.md5(login));
        if (user != null){
            String salt = user.getSalt();
            String pass = MD5Security.md5(password + salt);
            if (pass.equals(user.getPassword())){
                req.getSession().setAttribute("auth", MD5Security.md5(login));
                req.getSession().setAttribute("role", user.getRole());
                req.getSession().setAttribute("customerId", user.getCustomerid());
                if (!user.getRole().equals("ADMIN")) {
                    resp.sendRedirect("/customers/" + user.getCustomerid());
                    return;
                }
                resp.sendRedirect("/");
            }else{
                resp.sendRedirect("/login");
            }
        }
        else {
            resp.sendRedirect("/login");
        }
    }
}
