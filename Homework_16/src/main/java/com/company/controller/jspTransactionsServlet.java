package com.company.controller;

import com.company.domain.Account;
import com.company.domain.Transaction;
import com.company.service.AccountService;
import com.company.service.TransactionService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.regex.Pattern;

public class jspTransactionsServlet extends HttpServlet{

    private TransactionService transactionService = new TransactionService();
    private AccountService accountService = new AccountService();
    private Pattern patternTransactions = Pattern.compile("/transactions");
    private Pattern patternDelete = Pattern.compile("/transactions/delete");
    private Pattern patternCreate = Pattern.compile("/transactions/create");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (patternTransactions.matcher(req.getRequestURI()).matches()){
            req.setAttribute("transactions", transactionService.getAllTransactions());
            req.getRequestDispatcher("/WEB-INF/jsp/transactions.jsp").forward(req, resp);
        }
        else {
            resp.sendError(404);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String URI = req.getRequestURI();

        if (patternCreate.matcher(URI).matches()){
            Transaction transaction = new Transaction(
                    new BigDecimal(req.getParameter("Amount")),
                    req.getParameter("OperationType"),
                    Long.parseLong(req.getParameter("accountNumber"))
            );
            Account account = accountService.get(transaction.getAccountNumber());
            double original = account.getBalance().doubleValue();
            double update = Double.parseDouble(req.getParameter("Amount"));
            if (req.getParameter("OperationType").equalsIgnoreCase("wsd")){
                if (original < update){
                    resp.getWriter().write("Don't have much many!!!");
                }else if (update <= 0){
                    resp.getWriter().write("error");
                }
            }else if (req.getParameter("OperationType").equalsIgnoreCase("put")){
                if (update <= 0){
                    resp.getWriter().write("error");
                }
            }
            transactionService.save(transaction);
            double amount = req.getParameter("OperationType").equals("PUT")? original + update: original - update;
            account.setBalance(new BigDecimal(String.valueOf(amount)));
            accountService.update(account);
            req.getRequestDispatcher("/WEB-INF/jsp/rowBilder/transactionRow.jsp").forward(req, resp);
        }
        else if (patternDelete.matcher(URI).matches()){
            Long transactionId = Long.parseLong(req.getParameter("id").trim());
            boolean monitor = false;
            for (Transaction transaction : transactionService.getAllTransactions()){
                if (transaction.getId().equals(transactionId)){
                    transactionService.delete(transaction);
                    monitor = true;
                    break;
                }
            }
            if (monitor){
                resp.getWriter().write("success");
            }
            else {
                resp.sendError(500);
            }
        }
        else{
            resp.sendError(404);
        }
    }
}
