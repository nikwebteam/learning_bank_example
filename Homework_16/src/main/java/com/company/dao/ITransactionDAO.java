package com.company.dao;

import com.company.domain.Account;
import com.company.domain.Transaction;

import java.util.List;

public interface ITransactionDAO {

    Transaction get(Long accountNumber);
    void delete(Long accountId);
    Long save(Transaction account);
    void update(Transaction account);
    void delete(Transaction account);
    List<Transaction> list();
    List<Transaction> list(Account account);
    List<Transaction> list(Long accountNumber);
}
