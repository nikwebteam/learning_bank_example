package com.company.dao;

import com.company.domain.User;

public interface IUserDAO {

    User get(String login);
    Long save(User user);
    void delete(Long customerId);
    void update(User user);
}
