package com.company.dao;

import com.company.domain.User;
import com.company.util.ConnectionUtil;

import java.sql.*;

public class UserDAO implements IUserDAO{

    @Override
    public User get(String login) {
        String sql = "SELECT * FROM users WHERE login = ?";
        try (Connection connection = ConnectionUtil.create()) {
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                return extractUser(resultSet);
            }else{
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long save(User user) {
        String sql = "INSERT INTO users (login, password, salt, role, customerId)" +
                "VALUES (?, ?, ?, ?, ?)";
        try (Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,user.getLogin());
            statement.setString(2,user.getPassword());
            statement.setString(3,user.getSalt());
            statement.setString(4,user.getRole());
            statement.setLong(5,user.getCustomerid());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void delete(Long customerId) {
        String sql = "DELETE FROM users WHERE customerid = ?";
        try (Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setLong(1, customerId);
            statement.execute();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void update(User user) {
        String sql = "UPDATE users SET " +
                "login = ?, " +
                "password = ?, " +
                "salt = ?, " +
                "role = ? WHERE id = ?";
        try (Connection connection = ConnectionUtil.create()){
            PreparedStatement statement = connection.prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getSalt());
            statement.setString(4, user.getRole());
            statement.setLong(5, user.getId());
            statement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private User extractUser(ResultSet resultSet) throws SQLException {
        User user = new User(
                resultSet.getString("login"),
                resultSet.getString("password"),
                resultSet.getString("salt"),
                resultSet.getString("role"),
                resultSet.getLong("customerid")
        );
        user.setId(resultSet.getLong("id"));
        return user;
    }
}
