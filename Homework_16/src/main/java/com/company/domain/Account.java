package com.company.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Account {

    private Long id;
    private BigDecimal balance;
    private Timestamp creationDate;
    private String currency;
    private Boolean blocked;
    private Long customerId;

    public Account(BigDecimal balance, Timestamp creationDate, String currency, Boolean blocked, Long customerId) {
        this.balance = balance;
        this.creationDate = creationDate;
        this.currency = currency;
        this.blocked = blocked;
        this.customerId = customerId;
    }
    public Account(Long customerId, String currency, BigDecimal amount){
        this(amount, new Timestamp(System.currentTimeMillis()), currency, false, customerId);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", balance=" + balance +
                ", creationDate=" + creationDate +
                ", currency='" + currency + '\'' +
                ", blocked=" + blocked +
                ", customerId=" + customerId +
                '}';
    }
}
