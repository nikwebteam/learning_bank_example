package com.company.domain;


import java.util.List;
import java.util.Map;

public class Currency {

    private String organizationId;
    private Integer organizationType;
    private Long dateTime;
    private List<String> name;
    private List<String> city;
    private List<String> region;
    private Map<String,Map<String, Double>> rate;

    public Currency(String organizationId, Integer organizationType, Long dateTime, List<String> name, List<String> city, List<String> region, Map<String, Map<String, Double>> rate) {
        this.organizationId = organizationId;
        this.organizationType = organizationType;
        this.dateTime = dateTime;
        this.name = name;
        this.city = city;
        this.region = region;
        this.rate = rate;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(Integer organizationType) {
        this.organizationType = organizationType;
    }

    public Long getDateTime() {
        return dateTime;
    }

    public void setDateTime(Long dateTime) {
        this.dateTime = dateTime;
    }

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    public List<String> getCity() {
        return city;
    }

    public void setCity(List<String> city) {
        this.city = city;
    }

    public List<String> getRegion() {
        return region;
    }

    public void setRegion(List<String> region) {
        this.region = region;
    }

    public Map<String, Map<String, Double>> getRate() {
        return rate;
    }

    public void setRate(Map<String, Map<String, Double>> rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Currency currency = (Currency) o;

        if (organizationId != null ? !organizationId.equals(currency.organizationId) : currency.organizationId != null)
            return false;
        if (organizationType != null ? !organizationType.equals(currency.organizationType) : currency.organizationType != null)
            return false;
        if (dateTime != null ? !dateTime.equals(currency.dateTime) : currency.dateTime != null) return false;
        if (name != null ? !name.equals(currency.name) : currency.name != null) return false;
        if (city != null ? !city.equals(currency.city) : currency.city != null) return false;
        if (region != null ? !region.equals(currency.region) : currency.region != null) return false;
        return rate != null ? rate.equals(currency.rate) : currency.rate == null;

    }

    @Override
    public int hashCode() {
        int result = organizationId != null ? organizationId.hashCode() : 0;
        result = 31 * result + (organizationType != null ? organizationType.hashCode() : 0);
        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (region != null ? region.hashCode() : 0);
        result = 31 * result + (rate != null ? rate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "organizationId='" + organizationId + '\'' +
                ", organizationType=" + organizationType +
                ", dateTime=" + dateTime +
                ", name=" + name +
                ", city=" + city +
                ", region=" + region +
                ", rate=" + rate +
                '}';
    }
}
