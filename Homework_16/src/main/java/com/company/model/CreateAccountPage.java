package com.company.model;

import com.company.domain.Customer;
import com.company.domain.Transaction;
import com.company.domain.User;
import com.company.service.TransactionService;

import java.util.List;

public class CreateAccountPage {

    private static TransactionService transactionService = new TransactionService();

    public static String create(User user,Long accountNumber){
        List<Transaction> transactionList = transactionService.getAllTransactions(accountNumber);
        String page = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\">" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">" +
                "\t<script type=\"text/javascript\" src=\"../js/jquery-2.2.1.min.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"../js/script.js\"></script>" +
                "<title>Account</title></head><body><div class=\"main\">" +
                "<div class=\"header\">" +
                "\t\t\t<ul class=\"head_menu\">\n" +
                "\t\t\t\t<li><a href=\"/customers/" + user.getCustomerid() + "\"><img src=\"../images/back.png\"></a></li>\n" +
                "\t\t\t\t<li></li>\n" +
                "\t\t\t\t<li></li>\n" +
                "\t\t\t</ul>\n" +
                "\t\t\t<ul class=\"head_menu\">\n" +
                "\t\t\t\t<li style=\"float: right;\">\n" +
                "\t\t\t\t\t<a href=\"/logout\" class=\"log_out\" style=\"color:white;\">logout</a>\n" +
                "\t\t\t\t</li>\n" +
                "\t\t\t</ul>\n</div>" +
                "<div><div class=\"title\">" +
                "<span>Account "
                + accountNumber +
                "</div>" +
                "<div>" +
                "<form  id=\"add_transaction\" method=\"POST\" action=\"/\">" +
                "<div>" +
                "<label>Amount</label>" +
                "<input type=\"text\" id=\"Amount\" placeholder=\"Amount\" alt=\"Amount\"/>" +
                "<span class=\"required\" >This field can't be empty!</span>" +
                "<input type=\"hidden\" id=\"id\" placeholder=\"Amount\" alt=\"Amount\" value=\"" + accountNumber + "\" />" +
                "</div>" +
                "<div>" +
                "<label>Operation Type</label>" +
                "<select id=\"OperationType\">" +
                "<option value=\"\" disabled selected>Choose operation type</option>" +
                "<option value=\"PUT\">PUT</option>" +
                "    <option value=\"WSD\">WSD</option>" +
                "</select>" +
                "<span class=\"required\">This field can't be empty!</span>" +
                "</div>" +
                "<span class=\"create\">" +
                "<button type=\"button\"  name=\"save\" onclick=\"createTransaction()\">Create</button>" +
                "</span>" +
                "</form>" +
                "</div>" +
                "<div>" +
                "<span class=\"create\">" +
                "<button onclick=\"openTableTransaction()\">Create transaction</button>" +
                "</span>" +
                "</div>" +
                "<div><table class=\"tab\">" +
                "<thead class=\"t_head\"><tr><th>Amount</th><th>Date</th>" +
                "<th>Operation type</th><th>Account number</th>" +
                "<th>Delete</th></tr></thead><tbody>" +
                bildTr(transactionList)
                + "</tbody></table></div></div></div></body></html>";
        return page;
    }

    private static String bildTr(List<Transaction> transactionList){
        StringBuilder sb = new StringBuilder();
        for (Transaction transaction : transactionList){
            sb.append(CreateTR.transaction(transaction));
        }
        return sb.toString();
    }
}
