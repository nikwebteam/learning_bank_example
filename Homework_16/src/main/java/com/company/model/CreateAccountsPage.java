package com.company.model;

import com.company.domain.Account;
import com.company.service.AccountService;

import java.util.List;

public class CreateAccountsPage {

    private static AccountService accountService = new AccountService();

    public static String create(){
        String page = "<!DOCTYPE html>" +
                "<html><head><meta charset=\"UTF-8\">" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">" +
                "\t<script type=\"text/javascript\" src=\"../js/jquery-2.2.1.min.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"../js/script.js\"></script>" +
                "<title>Accounts</title></head><body><div class=\"main\">" +
                "<div class=\"header\"><ul class=\"head_menu\"><li>" +
                "<a href=\"/customers\">Customers</a></li><li>" +
                "<a href=\"/accounts\" class=\"active\">Accounts</a>" +
                "</li><li><a href=\"/transactions\">Transactions</a>" +
                "</li></ul><ul class=\"home\"><a href=\"/\">" +
                "<img src=\"../images/home.png\"></a></ul>" +
                "</div><div><div class=\"title\"><span>Accounts</span>" +
                "<a href=\"/logout\" class=\"log_out\">logout</a>" +
                "</div><div><table class=\"tab\"><thead class=\"t_head\">" +
                "<tr><th>Account number</th><th>Balance</th><th>Creation date</th>" +
                "<th>Currency</th><th>Blocked</th><th>Delete</th></tr></thead><tbody>" +
                bildTr()
                + "</tbody></table></div></div></div></body></html>";
        return page;
    }

    private static String bildTr(){
        StringBuilder sb = new StringBuilder();
        List<Account> accountList = accountService.getAllAccounts();
        for (Account account : accountList){
            sb.append(CreateTR.account(account) + "\n");
        }
        return sb.toString();
    }
}
