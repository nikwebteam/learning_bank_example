package com.company.model;

import com.company.domain.Account;
import com.company.domain.Customer;
import com.company.domain.User;
import com.company.service.AccountService;
import com.company.service.CustomerService;

import java.util.List;

public class CreateCustomerPage {

    private static AccountService accountService = new AccountService();
    private static CustomerService customerService = new CustomerService();

    public static String create(User user){
        Customer customer = null;
        for (Customer customers : customerService.getAllCustomers()){
            if (customers.getId().equals(user.getCustomerid())){
                customer = customers;
                break;
            }
        }
        String fullName = customer.getLastName() + " " + customer.getFirstName();
        List<Account> accountList = accountService.getAllAccounts(customer.getId());
        String page = "<!DOCTYPE html>" +
                "<html><head><meta charset=\"UTF-8\">" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">" +
                "\t<script type=\"text/javascript\" src=\"../js/jquery-2.2.1.min.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"../js/script.js\"></script>" +
                "<title>Customer</title>" +
                "</head><body><div class=\"main\"><div class=\"header\">" +
                "\t\t\t<ul class=\"head_menu\">\n" +
                "\t\t\t\t<li><a href=\"/customers\"><img src=\"../images/back.png\"></a></li>\n" +
                "\t\t\t\t<li></li>\n" +
                "\t\t\t\t<li></li>\n" +
                "\t\t\t</ul>\n" +
                "\t\t\t<ul class=\"head_menu\">\n" +
                "\t\t\t\t<li style=\"float: right;\">\n" +
                "\t\t\t\t\t<a href=\"/logout\" class=\"log_out\" style=\"color:white;\">logout</a>\n" +
                "\t\t\t\t</li>\n" +
                "\t\t\t</ul>\n</div>" +
                "<div><div class=\"title\"><span>" +
                fullName
                + "</span></div>" +
                "<div>" +
                "<form  id=\"add_account\" method=\"POST\" action=\"/\">" +
                "<div>" +
                "<label>Currency Type</label>" +
                "<select id=\"currencyType\">" +
                "<option value=\"\" disabled selected>Choose currency type</option>" +
                "<option value=\"EUR\">EUR</option>" +
                "<option value=\"USD\">USD</option>" +
                "<option value=\"UAN\">UAN</option>" +
                "</select>" +
                "<span class=\"required\">This field can't be empty!</span>" +
                "<input type=\"hidden\" id=\"id\" placeholder=\"Amount\" alt=\"Amount\" value=\"" + user.getCustomerid() + "\" />" +
                "</div>" +
                "<span class=\"create\">" +
                "<button type=\"button\"  name=\"save\" onclick=\"createAccount()\">Create</button>" +
                "</span>" +
                "</form>" +
                "</div><div>" +
                "<span class=\"create\">" +
                "<button onclick=\"openTableAccount()\">Create account</button>" +
                "</span>" +
                "</div>" +
                "<div><table class=\"tab\"><thead class=\"t_head\">" +
                "<tr><th>Account number</th><th>Balance</th><th>Creation date</th>" +
                "<th>Currency</th><th>Blocked</th><th>Delete</th></tr></thead><tbody>" +
                bildTr(accountList)
                + "</tbody></table></div></div></div></body></html>";
        return page;
    }

    private static String bildTr(List<Account> accountList){
        if (accountList.size() < 1){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Account account : accountList){
            sb.append(CreateTR.account(account) + "\n");
        }
        return sb.toString();
    }
}
