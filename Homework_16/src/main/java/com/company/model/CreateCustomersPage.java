package com.company.model;

import com.company.domain.Customer;
import com.company.service.CustomerService;

import java.util.List;

public class CreateCustomersPage {

    private static CustomerService customerService = new CustomerService();

    public static String create(){
        String page = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\">" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">" +
                "\t<script type=\"text/javascript\" src=\"../js/jquery-2.2.1.min.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"../js/script.js\"></script>" +
                "<title>Customers</title></head><body><div class=\"main\"><div class=\"header\">" +
                "<ul class=\"head_menu\"><li>" +
                "<a href=\"/customers\" class=\"active\">Customers</a></li><li>" +
                "<a href=\"/accounts\">Accounts</a></li><li>" +
                "<a href=\"/transactions\" >Transactions</a></li></ul>" +
                "<ul class=\"home\"><a href=\"/\">" +
                "<img src=\"../images/home.png\"></a></ul></div><div class=\"body\">" +
                "<div><div class=\"title\"><span>Customers</span>" +
                "<a href=\"/logout\" class=\"log_out\">logout</a>" +
                "</div><div><form class=\"add\" method=\"POST\">" +
                "<div><label>First name</label>" +
                "<input type=\"text\" id=\"firstName\" placeholder=\"First Name\" alt=\"First Name\"/>" +
                "<span class=\"required\">This field can't be empty!</span>" +
                "</div><div><label>Last name</label>" +
                "<input type=\"Last Name\" id=\"lastName\" placeholder=\"Last Name\" alt=\"Last Name\"/>" +
                "<span class=\"required\">This field can't be empty!</span>" +
                "</div><div><label>Birth date</label>" +
                "<input type=\"date\" id=\"birthDate\" placeholder=\"Birth Date\" alt=\"Birth Date\"/>" +
                "<span class=\"required\">This field can't be empty!</span>" +
                "</div><div><label>Address</label>" +
                "<input type=\"text\" id=\"address\" placeholder=\"Address\" alt=\"Address\"/>" +
                "<span class=\"required\">This field can't be empty!</span>" +
                "</div><div><label>City</label>" +
                "<input type=\"text\" id=\"city\" placeholder=\"City\" alt=\"City\"/>" +
                "<span class=\"required\">This field can't be empty!</span>" +
                "</div><div><label>Passport</label>" +
                "<input type=\"text\" id=\"passport\" placeholder=\"Passport\" alt=\"Passport\"/>" +
                "<span class=\"required\">This field can't be empty!</span>" +
                "</div><div><label>Phone</label>" +
                "<input type=\"text\" id=\"phone\" placeholder=\"Phone\" alt=\"Phone\"/>" +
                "<span class=\"required\" >This field can't be empty!</span>" +
                "</div><span class=\"create\"><button type=\"button\" name=\"save\" onclick=\"hideTable()\">Save</button>" +
                "</span></form></div><div><span class=\"create\">" +
                "<button onclick=\"openTable()\">Create customer</button>" +
                "</span></div></div><div><table class=\"tab\"><thead class=\"t_head\"><tr>" +
                "<th>First Name</th><th>Last Name</th><th>Birth Date</th>" +
                "<th>Address</th><th>City</th><th>Passport</th><th>Phone</th><th>Delete</th></tr></thead>" +
                "<tbody class=\"tbody\">" + bildString() + "</tbody></table></div></div></div></body></html>";
        return page;
    }
    private static String bildString(){
        StringBuilder sb = new StringBuilder();
        List<Customer> customers = customerService.getAllCustomers();
        for (Customer customer : customers){
            sb.append(CreateTR.customer(customer) + "\n");
        }
        return sb.toString();
    }
}
