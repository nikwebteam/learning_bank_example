package com.company.model;

public class CreateHomePage {

    public static String create(){
        String page = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"UTF-8\">\n" +
                " <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\"> " +
                "\t<script type=\"text/javascript\" src=\"../js/jquery-2.2.1.min.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/script.js\"></script>\n" +
                "\t<title>Home bank</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\t<div class=\"main\">\n" +
                "\t\t<div class=\"header\">\n" +
                "\t\t\t<ul class=\"head_menu\">\n" +
                "\t\t\t\t<li><a href=\"/customers\" >Customers</a></li>\n" +
                "\t\t\t\t<li><a href=\"/accounts\" >Accounts</a></li>\n" +
                "\t\t\t\t<li><a href=\"/transactions\" >Transactions</a></li>\n" +
                "\t\t\t</ul>\n" +
                "\t\t\t<ul class=\"head_menu\">\n" +
                "\t\t\t\t<li style=\"float: right;\">\n" +
                "\t\t\t\t\t<a href=\"/logout\" class=\"log_out\" style=\"color:white;\">logout</a>\n" +
                "\t\t\t\t</li>\n" +
                "\t\t\t</ul>\n" +
                "\t\t</div>\n" +
                "\t\t<div>\n" +
                "\t\t\t<div class=\"title\">\n" +
                "\t\t\t\t<span >Welcome to the Bank App!</span>" +
                "\t\t\t</div>\n" +
                "\t\t</div>\n" +
                "\t</div>\n" +
                "</body>\n" +
                "</html>";
        return page;
    }
}
