package com.company.model;

import java.security.spec.RSAPublicKeySpec;

public class CreateLoginPage {

    public static String create(RSAPublicKeySpec publicKeySpec){
        String page = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"UTF-8\">\n" +
                "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">\n" +
                "\t<script type=\"text/javascript\" src=\"../js/jquery-2.2.1.min.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/jsbn.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/jsbn2.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/prng4.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/rng.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/rsa.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/rsa2.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/script.js\"></script>" +
                "\t<title>Login Page</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\t<div class=\"main\" style=\"min-height: 500px;\">\n" +
                "\t\t<div class=\"header\">\n" +
                "\t\t\t<ul class=\"head_menu\">\n" +
                "\t\t\t\t<li><span>Login Page</span></li>\n" +
                "\t\t\t\t<li></li>\n" +
                "\t\t\t\t<li></li>\n" +
                "\t\t\t</ul>\n" +
                "\t\t\t<ul class=\"head_menu\">\n" +
                "\t\t\t\t<li style=\"float: right;\">\n" +
                "\t\t\t\t\t<a href=\"/registration\">Registration</a>\n" +
                "\t\t\t\t</li>\n" +
                "\t\t\t</ul>\n" +
                "\t\t</div>\n" +
                "\t\t<div id=\"login_table\">\n" +
                "\t\t\t<form  id=\"login_form\" method=\"POST\" action=\"/login\">\n" +
                "\t\t\t\t<div>\n" +
                "\t\t\t\t\t<label>User Name</label>\n" +
                "\t\t\t\t\t<input type=\"text\" name=\"login\" placeholder=\"login\" alt=\"Login\"/>\n" +
                "\t\t\t\t\t<span class=\"required\">This field can't be empty!</span>\n" +
                "\t\t\t\t</div>\n" +
                "\t\t\t\t<div>\n" +
                "\t\t\t\t\t<label>Password</label>\n" +
                "\t\t\t\t\t<input type=\"password\" name=\"password\" placeholder=\"password\" alt=\"Last Name\"/>\n" +
                "\t\t\t\t\t<span class=\"required\">This field can't be empty!</span>\n" +
                "\t\t\t\t\t<input type=\"hidden\" name=\"exponent\" value=\"10001\">\n" +
                "\t\t\t\t\t<input type=\"hidden\" name=\"module\" value=\"" + publicKeySpec.getModulus().toString(16) + "\">\n" +
                "\t\t\t\t</div>\n" +
                "\t\t\t\t<span class=\"btn_login\">\n" +
                "\t\t\t\t\t<button type=\"submit\"  name=\"btn_login\">Sign in</button>\n" +
                "\t\t\t\t</span>\n" +
                "\t\t\t</form>" +
                "\t\t</div>\n" +
                "\t</div>\n" +
                "</body>\n" +
                "</html>";
        return page;
    }
}
