package com.company.model;

import java.security.spec.RSAPublicKeySpec;

public class CreateRegisterPage {

    private String Role = "";

    public  String create(RSAPublicKeySpec publicKeySpec){
        String page = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"UTF-8\">\n" +
                "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">\n" +
                "\t<script type=\"text/javascript\" src=\"../js/jquery-2.2.1.min.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/jsbn.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/prng4.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/rng.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/rsa.js\"></script>\n" +
                "\t<script type=\"text/javascript\" src=\"../js/script.js\"></script>\n" +
                "\t<title>Customers</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\t<div class=\"main\">\n" +
                "\t\t<!-- This is begin HEADER   -->\n" +
                "\t\t<div class=\"header\">\n" +
                "\t\t\t<ul class=\"head_menu\">\n" +
                "\t\t\t\t<ul class=\"head_menu\">\n" +
                "\t\t\t\t<li style=\"color: white; margin-left: 40px;\">Registration Page</li>\n" +
                "\t\t\t\t<li></li>\n" +
                "\t\t\t\t<li></li>\n" +
                "\t\t\t</ul>\n" +
                "\t\t\t<span class=\"home_icon\">\n" +
                "\t\t\t\t<a href=\"/login\">\n" +
                "                \t<img src=\"../images/back.png\">\n" +
                "                </a>\n" +
                "            </span>\n" +
                "\t\t</div>\n" +
                "\t\t<!-- This is end of HEADER  -->\n" +
                "\t\t<div class=\"body\">\n" +
                "\t\t\t<div>\n" +
                "\t\t\t\t<div class=\"title\">\n" +
                "\t\t\t\t\t<span>Enter registration data</span>\n" +
                "\t\t\t\t</div>\n" +
                "\t\t\t\t<div>\n" +
                "\t\t\t\t\t<form  class=\"add\" style=\"display: block;\" method=\"POST\" action=\"#\">\n" +
                "\t\t\t\t\t\t<div>\n" +
                "\t\t\t\t\t\t\t<label>First name</label>\n" +
                "\t\t\t\t\t\t\t<input type=\"text\" name=\"firstName\" placeholder=\"First Name\" alt=\"First Name\"/>\n" +
                "\t\t\t\t\t\t\t<span class=\"required\">This field can't be empty!</span>\n" +
                "\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t<div>\n" +
                "\t\t\t\t\t\t\t<label>Last name</label>\n" +
                "\t\t\t\t\t\t\t<input type=\"Last Name\" name=\"lastName\" placeholder=\"Last Name\" alt=\"Last Name\"/>\n" +
                "\t\t\t\t\t\t\t<span class=\"required\">This field can't be empty!</span>\n" +
                "\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t<div>\n" +
                "\t\t\t\t\t\t\t<label>Birth date</label>\n" +
                "\t\t\t\t\t\t\t<input type=\"date\" name=\"birthDate\" placeholder=\"Birth Date\" alt=\"Birth Date\"/>\n" +
                "\t\t\t\t\t\t\t<span class=\"required\">This field can't be empty!</span>\n" +
                "\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t<div>\n" +
                "\t\t\t\t\t\t\t<label>Address</label>\n" +
                "\t\t\t\t\t\t\t<input type=\"text\" name=\"address\" placeholder=\"Address\" alt=\"Address\"/>\n" +
                "\t\t\t\t\t\t\t<span class=\"required\">This field can't be empty!</span>\n" +
                "\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t<div>\n" +
                "\t\t\t\t\t\t\t<label>City</label>\n" +
                "\t\t\t\t\t\t\t<input type=\"text\" name=\"city\" placeholder=\"City\" alt=\"City\"/>\n" +
                "\t\t\t\t\t\t\t<span class=\"required\">This field can't be empty!</span>\n" +
                "\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t<div>\n" +
                "\t\t\t\t\t\t\t<label>Passport</label>\n" +
                "\t\t\t\t\t\t\t<input type=\"text\" name=\"passport\" placeholder=\"Passport\" alt=\"Passport\"/>\n" +
                "\t\t\t\t\t\t\t<span class=\"required\">This field can't be empty!</span>\n" +
                "\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t<div>\n" +
                "\t\t\t\t\t\t\t<label>Phone</label>\n" +
                "\t\t\t\t\t\t\t<input type=\"text\" name=\"phone\" placeholder=\"Phone\" alt=\"Phone\"/>\n" +
                "\t\t\t\t\t\t\t<span class=\"required\" >This field can't be empty!</span>\n" +
                "\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t<div style=\"margin-bottom: 10px;\">\n" +
                "\t\t\t\t\t\t\t<label>login</label>\n" +
                "\t\t\t\t\t\t\t<input type=\"text\" name=\"login\" placeholder=\"login\" alt=\"login\"/>\n" +
                "\t\t\t\t\t\t\t<span class=\"required\" >This field can't be empty!</span>\n" +
                "\t\t\t\t\t<input type=\"hidden\" name=\"exponent\" value=\"10001\">\n" +
                "\t\t\t\t\t\t\t<input type=\"hidden\" name=\"module\" value=\"" + publicKeySpec.getModulus().toString(16) + "\">\n" +
                "\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t<div>\n" +
                "\t\t\t\t\t\t\t<label>Password</label>\n" +
                "\t\t\t\t\t\t\t<input type=\"password\" name=\"password\" placeholder=\"pasword\" alt=\"password\"/>\n" +
                "\t\t\t\t\t\t\t<span class=\"required\" >This field can't be empty!</span>\n" +
                "\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t<div>\n" +
                "\t\t\t\t\t\t\t<label>Re enter password</label>\n" +
                "\t\t\t\t\t\t\t<input type=\"password\" name=\"password2\" placeholder=\"pasword\" alt=\"password\"/>\n" +
                "\t\t\t\t\t\t\t<span class=\"required\" >This field can't be empty!</span>\n" +
                "\t\t\t\t\t\t</div>\n" +
                    Role +
                "\t\t\t\t\t\t<span class=\"create\">\n" +
                "\t\t\t\t\t\t\t<button type=\"submit\"  name=\"register\" onclick=\"registration()\">register</button>\n" +
                "\t\t\t\t\t\t</span>\n" +
                "\t\t\t\t\t</form>\n" +
                "\t\t\t\t</div>\n" +
                "\t\t\t</div>\n" +
                "\t</div>\n" +
                "</body>\n" +
                "</html>";
        return page;
    }

    public void setChooseRole(){
        String str = "<div>" +
                "<label>Role</label>" +
                "<select class=\"select_role\" name=\"role\">" +
                "<option value=\"\" disabled selected>choose role</option>" +
                "<option value=\"USER\">USER</option>" +
                "<option value=\"ADMIN\">ADMIN</option>" +
                "</select>" +
                "<span class=\"required\" >This field can't be empty!</span>" +
                "</div>";
        Role = str;
    }
}
