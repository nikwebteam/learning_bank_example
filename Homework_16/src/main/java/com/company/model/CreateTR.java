package com.company.model;

import com.company.domain.Account;
import com.company.domain.Customer;
import com.company.domain.Transaction;

public class CreateTR {

    public static String account(Account account){
        String tr = "<tr>" +
                        "<td>" +
                            "<a href=\"/accounts/" + account.getId() + "\">" + account.getId() + "</a>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + account.getBalance() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + account.getCreationDate().toString() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + account.getCurrency() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + account.getBlocked() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<a href=\"/accounts/delete\" value=\"" + account.getId() + "\" class=\"delete\" onclick=\"return false\">" +
                                "<img src=\"../images/delete.png\">" +
                            "</a>" +
                        "</td>" +
                        "</tr>";
        return tr;
    }

    public static String customer(Customer customer){
        String tr = "<tr>" +
                        "<td>" +
                            "<a href=\"/customers/" + customer.getId() + "\">" +
                                "<span>" + customer.getFirstName() + "</span>" +
                            "</a>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + customer.getLastName() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + customer.getBirthDate() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + customer.getAddress() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + customer.getCity() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + customer.getPassport() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + customer.getPhone() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<a href=\"/customers/delete\" value=\" "+ customer.getId() + "\" class=\"delete\" onclick=\"return false\">" +
                                "<img src=\"../images/delete.png\">" +
                            "</a>" +
                        "</td>" +
                   "</tr>";
        return tr;
    }

    public static String transaction(Transaction transaction){
        String tr = "<tr>" +
                        "<td>" +
                            "<span>" + transaction.getAmount() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + transaction.getDate().toString() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + transaction.getOperationType() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<span>" + transaction.getAccountNumber() + "</span>" +
                        "</td>" +
                        "<td>" +
                            "<a href=\"/transactions/delete\" value=\"" + transaction.getId() + "\" class=\"delete\" onclick=\"return false\">" +
                                "<img src=\"../images/delete.png\">" +
                            "</a>" +
                        "</td>" +
                    "</tr>";
        return tr;
    }
}
