package com.company.model;

import com.company.domain.Transaction;
import com.company.service.TransactionService;

import java.util.List;

public class CreateTransactionsPage {

    private static TransactionService transactionService = new TransactionService();

    public static String create(){
        String page = "<!DOCTYPE html>" +
                "<html><head><meta charset=\"UTF-8\">" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/style.css\">" +
                "\t<script type=\"text/javascript\" src=\"../js/jquery-2.2.1.min.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"../js/script.js\"></script>" +
                "<title>Transactions</title></head><body><div class=\"main\">" +
                "<div class=\"header\"><ul class=\"head_menu\"><li>" +
                "<a href=\"/customers\">Customers</a></li><li>" +
                "<a href=\"/accounts\">Accounts</a></li><li>" +
                "<a href=\"/transactions\" class=\"active\">Transactions</a></li></ul>" +
                "<ul class=\"home\"><a href=\"/\">" +
                "<img src=\"../images/home.png\"></a></ul></div>" +
                "<div><div class=\"title\"><span>Transactions</span>" +
                "<a href=\"/logout\" class=\"log_out\">logout</a>" +
                "</div><div><table class=\"tab\"><thead class=\"t_head\">" +
                "<tr><th>Amount</th><th>Date</th><th>Operation type</th>" +
                "<th>Account number</th><th>Delete</th></tr></thead><tbody>" +
                bildTr()
                + "</tbody></table></div>" +
                "</div></div></body></html>";
        return page;
    }

    private static String bildTr(){
        StringBuilder sb = new StringBuilder();
        List<Transaction> transactionList = transactionService.getAllTransactions();
        for (Transaction transaction : transactionList){
            sb.append(CreateTR.transaction(transaction));
        }
        return sb.toString();
    }
}
