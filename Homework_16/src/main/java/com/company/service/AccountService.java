package com.company.service;

import com.company.dao.AccountDAO;
import com.company.dao.IAccountDAO;
import com.company.domain.Account;
import com.company.domain.Customer;
import com.company.domain.Transaction;

import java.math.BigDecimal;
import java.util.List;

public class AccountService {

    private IAccountDAO accountDAO = new AccountDAO();
    private TransactionService transactionService = new TransactionService();

    public Account save(Account account){
        Long id = accountDAO.save(account);
        if (!account.getBalance().equals(BigDecimal.ZERO)){
            transactionService.save(new Transaction(account.getBalance(), "PUT", id));
        }
        return accountDAO.get(id);
    }

    public void update(Account account){
        accountDAO.update(account);
    }

    public Account get(Long accountNumber){
       return accountDAO.get(accountNumber);
    }

    public List<Account> getAllAccounts(){
        return accountDAO.list();
    }

    public List<Account> getAllAccounts(Long customerId){
        return accountDAO.list(customerId);
    }

    public void delete(Account account){
        transactionService.delete(account);
        accountDAO.delete(account);
    }

    public void deleteAll(Customer customer){
        for (Account account : accountDAO.list()){
            if (account.getCustomerId().equals(customer.getId())) {
                delete(account);
            }
        }
    }
}
