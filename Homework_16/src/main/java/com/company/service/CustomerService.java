package com.company.service;


import com.company.dao.CustomerDAO;
import com.company.dao.ICustomerDAO;
import com.company.domain.Account;
import com.company.domain.Customer;

import java.math.BigDecimal;
import java.util.List;

public class CustomerService {

    private ICustomerDAO customerDAO = new CustomerDAO();
    private AccountService accountService = new AccountService();
    private UserService userService = new UserService();

    public Long save(Customer customer){
        Long id = customerDAO.save(customer);
        if (id != null) {
            accountService.save(new Account(id, "UAN", new BigDecimal(0)));
        }
        return id;
    }

    public Customer get(Long accountNumber){
        return customerDAO.get(accountNumber);
    }

    public Customer getById(Long customerId){
        for (Customer customer : customerDAO.customersList()){
            if (customer.getId().equals(customerId)){
                return customer;
            }
        }
        return null;
    }

    public List<Customer> getAllCustomers(){
        return customerDAO.customersList();
    }

    public void delete(Customer customer){
        accountService.deleteAll(customer);
        userService.delete(customer.getId());
        customerDAO.delete(customer);
    }

}
