package com.company.service;

import com.company.dao.ITransactionDAO;
import com.company.dao.TransactionDAO;
import com.company.domain.Account;
import com.company.domain.Transaction;

import java.util.List;

public class TransactionService {

    private ITransactionDAO transactionDAO = new TransactionDAO();

    public Transaction save(Transaction transaction){
        Long id = transactionDAO.save(transaction);
        return transactionDAO.get(id);
    }

    public List<Transaction> getAllTransactions(){
        return transactionDAO.list();
    }
    public List<Transaction> getAllTransactions(Long accountNumber){
        return transactionDAO.list(accountNumber);
    }

    public void delete(Transaction transaction){
        transactionDAO.delete(transaction);
    }

    public void delete(Account account){
        transactionDAO.delete(account.getId());
    }
}
