package com.company.service;

import com.company.dao.IUserDAO;
import com.company.dao.UserDAO;
import com.company.domain.User;

public class UserService {

    private IUserDAO userDAO = new UserDAO();

    public void save(User user){
        userDAO.save(user);
    }

    public User get(String login){
        return userDAO.get(login);
    }

    public void delete(Long customerId){
        userDAO.delete(customerId);
    }

    public void update(User user){
        userDAO.update(user);
    }

}
