package com.company.util;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CurrencyParser {

    public static List<String> parse(String URL) {

        Map<String, String> org_type = new HashMap<>(),
                            currencies = new HashMap<>(),
                            regions = new HashMap<>(),
                            cities = new HashMap<>();
        try {
            URL url = new URL(URL);
            URLConnection connection = url.openConnection();

            Document document = parseXML(connection.getInputStream());

            Map<String, NodeList> nodesMap = new HashMap<>();

            nodesMap.put("organization",document.getElementsByTagName("organization"));
            nodesMap.put("org_type",document.getElementsByTagName("org_types").item(0).getChildNodes());
            nodesMap.put("currencies",document.getElementsByTagName("currencies").item(0).getChildNodes());
            nodesMap.put("regions",document.getElementsByTagName("regions").item(0).getChildNodes());
            nodesMap.put("cities",document.getElementsByTagName("cities").item(0).getChildNodes());

            for (Map.Entry<String, NodeList> pair : nodesMap.entrySet())
            {
                for (int i = 0; i < pair.getValue().getLength(); i++)
                {
                    NamedNodeMap attr = null;
                    if (pair.getKey().equals("org_type")){
                        attr = pair.getValue().item(i).getAttributes();
                        org_type.put(attr.getNamedItem("id").getNodeValue(),attr.getNamedItem("title").getNodeValue());
                    }
                    else if (pair.getKey().equals("currencies")){
                        attr = pair.getValue().item(i).getAttributes();
                        currencies.put(attr.getNamedItem("id").getNodeValue(),attr.getNamedItem("title").getNodeValue());
                    }
                    else if (pair.getKey().equals("regions")){
                        attr = pair.getValue().item(i).getAttributes();
                        regions.put(attr.getNamedItem("id").getNodeValue(),attr.getNamedItem("title").getNodeValue());
                    }
                    else if (pair.getKey().equals("cities")){
                        attr = pair.getValue().item(i).getAttributes();
                        cities.put(attr.getNamedItem("id").getNodeValue(),attr.getNamedItem("title").getNodeValue());
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private static Document parseXML(InputStream stream)
            throws Exception
    {
        DocumentBuilderFactory objDocumentBuilderFactory = null;
        DocumentBuilder objDocumentBuilder = null;
        Document doc = null;
        try
        {
            objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

            doc = objDocumentBuilder.parse(stream);
        }
        catch(Exception ex)
        {
            throw ex;
        }

        return doc;
    }
}
