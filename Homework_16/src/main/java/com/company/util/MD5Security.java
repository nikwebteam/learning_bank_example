package com.company.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class MD5Security {

    public static String md5(String input) {

        String md5 = null;
        if(null == input) return null;
        try {
            //Create MessageDigest object for MD5
            MessageDigest digest = MessageDigest.getInstance("MD5");

            //Update input string in message digest
            digest.update(input.getBytes(), 0, input.length());

            //Converts message digest value in base 16 (hex)
            md5 = new BigInteger(1, digest.digest()).toString(16);

        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        }
        return md5;
    }

    public static String createSalt(){
        Random random = new Random();
        String result = "";
        int count = random.nextInt((49 - 20) + 1) + 20;
        int monitor = 0;
        while (monitor < count) {
            Long rand = random.nextLong();
            if (rand < 0){
                rand = - rand;
            }
            if (rand.byteValue() > 96 && rand.byteValue() < 123
                    || rand.byteValue() > 64 && rand.byteValue() < 91
                    || rand.byteValue() > 47 && rand.byteValue() < 58){
                result += (char) rand.byteValue();
                monitor++;
            }
        }
        return result;
    }
}
