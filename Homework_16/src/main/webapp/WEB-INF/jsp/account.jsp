<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script type="text/javascript" src="../js/jquery-2.2.1.min.js"></script>
        <script type="text/javascript" src="../js/script.js"></script>
        <title>Account</title>
    </head>
    <body>
    <div class="main">
        <div class="header">
            <ul class="head_menu">
                <li><a href="/customers/${customerId}"><img src="../images/back.png"></a></li>
                <li></li>
                <li></li>
            </ul>
            <ul class="head_menu">
                <li style="float: right;">
                    <a href="/logout" class="log_out" style="color:white;">logout</a>
                </li>
            </ul>
        </div>
        <div>
            <div class="title">
                <span>Account ${accountNumber}</span>
            </div>
            <div>
                <form id="add_transaction" method="POST" action="/">
                    <div>
                        <label>Amount</label>
                        <input type="text" id="Amount" placeholder="Amount" alt="Amount"/>
                        <span class="required">This field can't be empty!</span>
                        <input type="hidden" id="id" placeholder="Amount" alt="Amount" value="${accountNumber}"/>
                    </div>
                    <div>
                        <label>Operation Type</label>
                        <select id="OperationType">
                            <option value="" disabled selected>Choose operation type</option>
                            <option value="PUT">PUT</option>
                            <option value="WSD">WSD</option>
                        </select>
                        <span class="required">This field can't be empty!</span>
                    </div>
                    <span class="create">
                        <button type="button" name="save" onclick="createTransaction()">Create</button>
                    </span>
                </form>
            </div>
            <div>
                <span class="create">
                    <button onclick="openTableTransaction()">Create transaction</button>
                </span>
            </div>
            <div>
                <table class="tab">
                    <thead class="t_head">
                    <tr>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Operation type</th>
                        <th>Account number</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                        <c:if test="${transactions != null}">
                            <c:forEach items="${transactions}" var="transaction">
                                <tr>
                                    <td>
                                        <span>${transaction.getAmount()}</span>
                                    </td>
                                    <td>
                                        <span>${transaction.getDate()}</span>
                                    </td>
                                    <td>
                                        <span>${transaction.getOperationType()}</span>
                                    </td>
                                    <td>
                                        <span>${transaction.getAccountNumber()}</span>
                                    </td>
                                    <td>
                                        <a href="/transactions/delete" value="${transaction.getId()}" class="delete" onclick="return false">
                                            <img src="../images/delete.png">
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </body>
</html>
