<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script type="text/javascript" src="../js/jquery-2.2.1.min.js"></script>
        <script type="text/javascript" src="../js/script.js"></script>
        <title>Accounts</title>
    </head>
    <body>
    <div class="main">
        <c:import url="header.jsp"/>
        <div>
            <div class="title"><span>Accounts</span>
                <a href="/logout" class="log_out">logout</a>
            </div>
            <div>
                <table class="tab">
                    <thead class="t_head">
                    <tr>
                        <th>Account number</th>
                        <th>Balance</th>
                        <th>Creation date</th>
                        <th>Currency</th>
                        <th>Blocked</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${accounts}" var="account">
                        <tr>
                            <td>
                                <a href="/accounts/${account.getId()}">${account.getId()}</a>
                            </td>
                            <td>
                                <span>${account.getBalance()}</span>
                            </td>
                            <td>
                                <span>${account.getCreationDate().toString()}</span>
                            </td>
                            <td>
                                <span>${account.getCurrency()}</span>
                            </td>
                            <td>
                                <span>${account.getBlocked()}</span>
                            </td>
                            <td>
                                <a href="/accounts/delete" value="${account.getId()}" class="delete" onclick="return false">
                                    <img src="../images/delete.png">
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </body>
</html>
