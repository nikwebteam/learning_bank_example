<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script type="text/javascript" src="../js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="../js/script.js"></script>
    <title>Customer</title>
</head>
    <body>
    <div class="main">
        <div class="header">
            <ul class="head_menu">
                <li><a href="/customers"><img src="../images/back.png"></a></li>
                <li></li>
                <li></li>
            </ul>
            <ul class="head_menu">
                <li style="float: right;">
                    <a href="/logout" class="log_out" style="color:white;">logout</a>
                </li>
            </ul>
        </div>
        <div>
            <div class="title">
                <span>
                ${fullName}
                </span>
            </div>
            <div>
                <form id="add_account" method="POST" action="/">
                    <div>
                        <label>Currency Type</label>
                        <select id="currencyType">
                            <option value="" disabled selected>Choose currency type</option>
                            <option value="EUR">EUR</option>
                            <option value="USD">USD</option>
                            <option value="UAN">UAN</option>
                        </select>
                        <span class="required">This field can't be empty!</span>
                        <input type="hidden" id="id" placeholder="Amount" alt="Amount" value="${customerId}"/>
                    </div>
                    <span class="create">
                        <button type="button" name="save" onclick="createAccount()">Create</button>
                    </span>
                </form>
            </div>
            <div>
                <span class="create">
                    <button onclick="openTableAccount()">Create account</button>
                </span>
            </div>
            <div>
                <table class="tab">
                    <thead class="t_head">
                    <tr>
                        <th>Account number</th>
                        <th>Balance</th>
                        <th>Creation date</th>
                        <th>Currency</th>
                        <th>Blocked</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                        <c:if test="${accounts != null}">
                            <c:forEach items="${accounts}" var="account">
                                <tr>
                                    <td>
                                        <a href="/accounts/${account.getId()}">${account.getId()}</a>
                                    </td>
                                    <td>
                                        <span>${account.getBalance()}</span>
                                    </td>
                                    <td>
                                        <span>${account.getCreationDate()}</span>
                                    </td>
                                    <td>
                                        <span>${account.getCurrency()}</span>
                                    </td>
                                    <td>
                                        <span>${account.getBlocked()}</span>
                                    </td>
                                    <td>
                                        <a href="/accounts/delete" value="${account.getId()}" class="delete" onclick="return false">
                                            <img src="../images/delete.png">
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </body>
</html>
