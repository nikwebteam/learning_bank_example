<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script type="text/javascript" src="../js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="../js/script.js"></script>
    <title>Customers</title>
    </head>
<body>
<div class="main">
    <c:import url="header.jsp"/>
    <div class="body">
        <div>
            <div class="title"><span>Customers</span>
                <a href="/logout" class="log_out">logout</a>
            </div>
            <div>
                <form class="add" method="POST" action="/registration">
                    <div>
                        <label>First name</label>
                        <input type="text" id="firstName" placeholder="First Name" alt="First Name"/>
                        <span class="required">This field can't be empty!</span>
                    </div>
                    <div>
                        <label>Last name</label>
                        <input type="Last Name" id="lastName" placeholder="Last Name" alt="Last Name"/>
                        <span class="required">This field can't be empty!</span>
                    </div>
                    <div>
                        <label>Birth date</label>
                        <input type="date" id="birthDate" placeholder="Birth Date" alt="Birth Date"/>
                        <span class="required">This field can't be empty!</span>
                    </div>
                    <div>
                        <label>Address</label>
                        <input type="text" id="address" placeholder="Address" alt="Address"/>
                        <span class="required">This field can't be empty!</span>
                    </div>
                    <div>
                        <label>City</label>
                        <input type="text" id="city" placeholder="City" alt="City"/>
                        <span class="required">This field can't be empty!</span>
                    </div>
                    <div>
                        <label>Passport</label>
                        <input type="text" id="passport" maxlength="8" placeholder="Passport" alt="Passport"/>
                        <span class="required">This field can't be empty!</span>
                    </div>
                    <div>
                        <label>Phone</label>
                        <input type="text" id="phone" placeholder="Phone" alt="Phone"/>
                        <span class="required">This field can't be empty!</span>
                    </div>
                     <div style="margin-bottom: 10px;">
                    <label>login</label>
                    <input type="text" name="login" placeholder="login" alt="login"/>
                    <span class="required">This field can't be empty!</span>
                    </div>
                    <div>
                        <label>Password</label>
                        <input type="password" name="password" placeholder="pasword" alt="password"/>
                        <span class="required">This field can't be empty!</span>
                    </div>
                    <div>
                        <label>Re enter password</label>
                        <input type="password" name="password2" placeholder="pasword" alt="password"/>
                        <span class="required">This field can't be empty!</span>
                    </div>
                    <div>
                        <label>Role</label>
                        <select class="select_role" name="role">
                        <option value="" disabled selected>choose role</option>
                        <option value="USER">USER</option>
                        <option value="ADMIN">ADMIN</option>
                    </select>
                    <span class="required" >This field can't be empty!</span>
                    </div>
                    <span class="create">
                        <button type="button" name="save" onclick="hideTable()">Save</button>
                    </span>
                </form>
            </div>
            <div>
                <span class="create">
                    <button onclick="openTable()">Create customer</button>
                </span>
            </div>
        </div>
        <div>
            <table class="tab">
                <thead class="t_head">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Birth Date</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>Passport</th>
                    <th>Phone</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody class="tbody">
                <c:forEach items="${customers}" var="customer">
                    <tr>
                        <td>
                            <a href="/customers/${customer.getId()}">
                                <span>${customer.getFirstName()}</span>
                            </a>
                        </td>
                        <td>
                            <span>${customer.getLastName()}</span>
                        </td>
                        <td>
                            <span>${customer.getBirthDate()}</span>
                        </td>
                        <td>
                            <span>${customer.getAddress()}</span>
                        </td>
                        <td>
                            <span>${customer.getCity()}</span>
                        </td>
                        <td>
                            <span>${customer.getPassport()}</span>
                        </td>
                        <td>
                            <span>${customer.getPhone()}</span>
                        </td>
                        <td>
                            <a href="/customers/delete" value="${customer.getId()}" class="delete" onclick="return false">
                                <img src="../images/delete.png" alt="delete">
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>

