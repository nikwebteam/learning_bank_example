<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="header">
    <ul class="head_menu">
        <li>
            <a href="/customers" <c:if test="${customers != null}"> class="active" </c:if>>Customers</a>
        </li>
        <li>
            <a href="/accounts" <c:if test="${accounts != null}"> class="active" </c:if>>Accounts</a>
        </li>
        <li>
            <a href="/transactions" <c:if test="${transactions != null}"> class="active" </c:if>>Transactions</a>
        </li>
    </ul>
    <ul class="home">
        <a href="/">
            <img src="../images/home.png">
        </a>
    </ul>
</div>
