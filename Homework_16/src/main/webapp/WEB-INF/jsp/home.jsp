<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="../js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="../js/script.js"></script>
    <title>Home bank</title>
</head>
    <body>
    <div class="main">
        <div class="header">
            <ul class="head_menu">
                <li><a href="/customers">Customers</a></li>
                <li><a href="/accounts">Accounts</a></li>
                <li><a href="/transactions">Transactions</a></li>
            </ul>
            <ul class="head_menu">
                <li style="float: right;">
                    <a href="/logout" class="log_out" style="color:white;">logout</a>
                </li>
            </ul>
        </div>
        <div>
            <div class="title">
                <span>Welcome to the Bank App!</span>
            </div>
        </div>
    </div>
    </body>
</html>
