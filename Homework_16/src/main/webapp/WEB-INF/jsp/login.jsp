<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html> 
<html> 
<head> 
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <title>Login Page</title> 
</head>
    <body>
    <div class="main" style="min-height: 500px;">
        <div class="header">
            <ul class="head_menu">
                <li><span>Login Page</span></li>
                <li></li>
                <li></li>
            </ul>
            <ul class="head_menu">
                <li style="float: right;">
                    <a href="/registration">Registration</a>
                </li>
            </ul>
        </div>
        <div id="login_table">
            <form id="login_form" method="POST" action="/login">
                <div>
                    <label>User Name</label>
                    <input type="text" name="login" placeholder="login" alt="Login"/>
                    <span class="required">This field can't be empty!</span>
                </div>
                <div>
                    <label>Password</label>
                    <input type="password" name="password" placeholder="password" alt="Last Name"/>
                    <span class="required">This field can't be empty!</span>
                </div>
                <span class="btn_login">
                    <button>Sign in</button>
                </span>
            </form>
        </div>
    </div>
    </body>
</html>
