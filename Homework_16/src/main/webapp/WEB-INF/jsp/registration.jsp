<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script type="text/javascript" src="../js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="../js/script.js"></script>
    <title>Registration</title>
</head>
    <body>
    <div class="main">
        <div class="header">
            <ul class="head_menu">
                <li>
                    <span>Registration Page</span>
                </li>
                <li></li>
                <li></li>
            </ul>
            <ul class="head_menu">
                <li style="float: right;">
                    <a href="/login">Login</a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div>
                <div class="title">
                    <span>Enter registration data</span>
                </div>
                <div>
                    <form class="add" style="display: block;" method="POST" action="/registration">
                        <div>
                            <label>First name</label>
                            <input type="text" name="firstName" placeholder="First Name" alt="First Name"/>
                            <span class="required">This field can't be empty!</span>
                        </div>
                        <div>
                            <label>Last name</label>
                            <input type="Last Name" name="lastName" placeholder="Last Name" alt="Last Name"/>
                            <span class="required">This field can't be empty!</span>
                        </div>
                        <div>
                            <label>Birth date</label>
                            <input type="date" name="birthDate" placeholder="Birth Date" alt="Birth Date"/>
                            <span class="required">This field can't be empty!</span>
                        </div>
                        <div>
                            <label>Address</label>
                            <input type="text" name="address" placeholder="Address" alt="Address"/>
                            <span class="required">This field can't be empty!</span>
                        </div>
                        <div>
                            <label>City</label>
                            <input type="text" name="city" placeholder="City" alt="City"/>
                            <span class="required">This field can't be empty!</span>
                        </div>
                        <div>
                            <label>Passport</label>
                            <input type="text" name="passport" maxlength="8" placeholder="Passport" alt="Passport"/>
                            <span class="required">This field can't be empty!</span>
                        </div>
                        <div>
                            <label>Phone</label>
                            <input type="text" name="phone" placeholder="Phone" alt="Phone"/>
                            <span class="required">This field can't be empty!</span>
                        </div>
                        <div style="margin-bottom: 10px;">
                            <label>login</label>
                            <input type="text" name="login" placeholder="login" alt="login"/>
                            <span class="required">This field can't be empty!</span>
                        </div>
                        <div>
                            <label>Password</label>
                            <input type="password" name="password" placeholder="pasword" alt="password"/>
                            <span class="required">This field can't be empty!</span>
                        </div>
                        <div>
                            <label>Re enter password</label>
                            <input type="password" name="password2" placeholder="pasword" alt="password"/>
                            <span class="required">This field can't be empty!</span>
                        </div>
                        <span class="create">
                            <button type="button" name="register">register</button>
                         </span>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </body>
</html>
