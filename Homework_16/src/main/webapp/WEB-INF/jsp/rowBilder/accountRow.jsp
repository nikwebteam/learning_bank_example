<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<tr>
    <td>
        <a href="/accounts/${account.getId()}">${account.getId()}</a>
    </td>
    <td>
        <span>${account.getBalance()}</span>
    </td>
    <td>
        <span>${account.getCreationDate().toString()}</span>
    </td>
    <td>
        <span>${account.getCurrency()}</span>
    </td>
    <td>
        <span>${account.getBlocked()}</span>
    </td>
    <td>
        <a href="/accounts/delete" value="${account.getId()}" class="delete" onclick="return false">
            <img src="../images/delete.png">
        </a>
    </td>
</tr>
