<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <tr>
        <td>
            <a href="/customers/${customer.getId()}">
                <span>${customer.getFirstName()}</span>
            </a>
        </td>
        <td>
            <span>${customer.getLastName()}</span>
        </td>
        <td>
            <span>${customer.getBirthDate()}</span>
        </td>
        <td>
            <span>${customer.getAddress()}</span>
        </td>
        <td>
            <span>${customer.getCity()}</span>
        </td>
        <td>
            <span>${customer.getPassport()}</span>
        </td>
        <td>
            <span>${customer.getPhone()}</span>
        </td>
        <td>
            <a href="/customers/delete" value="${customer.getId()}" class="delete" onclick="return false">
                <img src="../images/delete.png" alt="delete">
            </a>
        </td>
    </tr>
