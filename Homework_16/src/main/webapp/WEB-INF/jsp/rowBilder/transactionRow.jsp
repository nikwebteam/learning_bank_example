<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<tr>
    <td>
        <span>${transaction.getAmount()}</span>
    </td>
    <td>
        <span>${transaction.getDate()}</span>
    </td>
    <td>
        <span>${transaction.getOperationType()}</span>
    </td>
    <td>
        <span>${transaction.getAccountNumber()}</span>
    </td>
    <td>
        <a href="/transactions/delete" value="${transaction.getId()}" class="delete" onclick="return false">
            <img src="../images/delete.png">
        </a>
    </td>
</tr>
