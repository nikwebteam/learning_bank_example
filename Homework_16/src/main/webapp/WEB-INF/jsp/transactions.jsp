<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script type="text/javascript" src="../js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="../js/script.js"></script>
    <title>Transactions</title>
</head>
    <body>
    <div class="main">
        <c:import url="header.jsp"/>
        <div>
            <div class="title"><span>Transactions</span>
                <a href="/logout" class="log_out">logout</a>
            </div>
            <div>
                <table class="tab">
                    <thead class="t_head">
                        <tr>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Operation type</th>
                            <th>Account number</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${transactions}" var="transaction">
                            <c:if test="${transaction != null}">
                                <tr>
                                    <td>
                                        <span>${transaction.getAmount()}</span>
                                    </td>
                                    <td>
                                        <span>${transaction.getDate().toString()}</span>
                                    </td>
                                    <td>
                                        <span>${transaction.getOperationType()}</span>
                                    </td>
                                    <td>
                                        <span>${transaction.getAccountNumber()}</span>
                                    </td>
                                    <td>
                                        <a href="/transactions/delete" value="${transaction.getId()}"  class="delete" onclick="return false">
                                            <img src="../images/delete.png">
                                        </a>
                                    </td>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </body>
</html>
