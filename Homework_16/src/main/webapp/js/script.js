function openTable() {
    document.getElementsByClassName("add")[0].style.display = 'block';
    document.getElementsByClassName("create")[1].style.display = 'none';

	var inputs = document.querySelectorAll(".add input");

	for (var i = 0; i < inputs.length; i++) {
		inputs[i].addEventListener("blur", requiredAll);
	}
}

function hideTable() {
	var list = getAllElements();
	var monitor = false;
	for(var i = 0; i < list.length; i++){
		if(list[i].value == ''){
			list[i].nextElementSibling.style.display = 'block';
		}else{monitor = true;}
	}
	if(monitor){
		var password = $('input[name=password]').val();
		var password2 = $('input[name=password2]').val();

		if (!(password === password2)) {
			$('input[name=password]').addClass('exist');
			$('input[name=password2]').addClass('exist');
		}
		else {
			var firstName = document.getElementById("firstName").value;
			var lastName = document.getElementById("lastName").value;
			var birthDate = document.getElementById("birthDate").value;
			var address = document.getElementById("address").value;
			var city = document.getElementById("city").value;
			var passport = document.getElementById("passport").value;
			var phone = document.getElementById("phone").value;
			var login = $('input[name=login]').val();
			var password = $('input[name=password]').val();
			var role = $('select[name=role]').val();

			$.post("/registration",
				{
					firstName: firstName,
					lastName: lastName,
					birthDate: birthDate,
					address: address,
					city: city,
					passport: passport,
					phone: phone,
					login: login,
					password: password,
					role: role
				},
				function (data) {
					var tr = data;
					var bod1 = document.getElementsByTagName("tbody")[0];
					bod1.innerHTML += tr;
					document.getElementsByClassName("add")[0].style.display = 'none';
					document.getElementsByClassName("add")[0].reset();
					document.getElementsByClassName("create")[1].style.display = 'block';
					deleteRow();
				});
		}
	}
}

function openTableAccount(){
	document.getElementById("add_account").style.display = 'block';
	document.getElementsByClassName("create")[1].style.display = 'none';

	var select = document.getElementById("currencyType");
	select.addEventListener("blur", requiredAll);
}

function createTransaction(){
	var spans = document.querySelectorAll(".required");
	var OperationType = document.getElementById("OperationType").value;
	var Amount = document.getElementById("Amount").value;
	var monitor = true;
	if(OperationType == '' || Amount == ''){
		monitor = false;
		for (var i = 0; i < inputs.length; i++) {
			spans[i].style.display = 'block';
		}
	}
	if(monitor){
	var accountNumber = document.getElementById("id").value;
	$.post("/transactions/create",
        				{
        					Amount:Amount,
							OperationType:OperationType,
							accountNumber:accountNumber
        				},
        				function(data){
        					var bod = document.getElementsByTagName("tbody")[0];
        					if(data == 'error'){
        						alert('Wrong sum!!!');
        					}else if(data == 'Don\'t have much many!!!'){
        						alert(data);
        					}else{
								bod.innerHTML += data;
								document.getElementById("add_transaction").style.display = 'none';
								document.getElementById("add_transaction").reset();
								document.getElementsByClassName("create")[1].style.display = 'block';
								deleteRow();
        					}
        				});
    }
}

function createAccount(){
	var spans = document.querySelectorAll(".required");
	var currencyType = document.getElementById("currencyType").value;
	var monitor = true;
	if(currencyType == ''){
		monitor = false;
		spans[0].style.display = 'block';
	}
	if(monitor){
	var customerId = document.getElementById("id").value;
	$.post("/accounts/create",
    				{
    					customerId:customerId,
    					currencyType:currencyType
    				},
    				function(data){
    					var bod = document.getElementsByTagName("tbody")[0];
    					bod.innerHTML += data;
    					document.getElementById("add_account").style.display = 'none';
    					document.getElementById("add_account").reset();
    					document.getElementsByClassName("create")[1].style.display = 'block';
    					deleteRow();
    				});
    }
}

function openTableTransaction(){
	document.getElementById("add_transaction").style.display = 'block';
	document.getElementsByClassName("create")[1].style.display = 'none';
	var rows = document.getElementById("add_transaction").rows;
	var select = document.getElementById("OperationType");
	var input = document.getElementById("Amount");
	input.addEventListener("blur", requiredAll);
	select.addEventListener("blur", requiredAll);

}

function deleteRow(){

	var rows = document.querySelectorAll("td > a.delete");

	for (var i = 0; i < rows.length; i++) {
		rows[i].addEventListener("click", function(event){

		var elem = event.currentTarget;
		var url = $(elem).attr("href");
		var data = $(elem).attr("value");
		$.post(url, { id:data }, function(data, status){
								if(status == data){
									elem = elem.parentNode.parentNode;
									elem.remove();
								}
							});

		});
	}
	registration();
	requiredRegister();
}

    function requiredAll(event){
        var tmp = event.currentTarget;
        var next = tmp.nextElementSibling;
        if(tmp.value == ''){
            next.style.display = 'block';
        }else{
            if(next.style.display == 'block'){
                next.style.display = 'none';
            }
        }
    }

    function requiredRegister(){
    	var list = getAllElements();
    	for (var i = 0; i < list.length; i++) {
    		list[i].addEventListener("blur", requiredAll);
    	}
    }

    function getAllElements() {
       var elements = [];
       ['input', 'select'].forEach(function (nodeName) {
           elements.push.apply(elements , document.getElementsByTagName(nodeName));
       });
       return elements;
    }

    function registration(){
			$('input[name=login]').on('blur',function(){
				var log = $('input[name=login]').val();
				$.post("/registration",
					{
						login:log
					},
					function(data){
						if (data == "false"){
							$('input[name=login]').addClass('exist');
							$('button[name=register]').prop("disabled", true);
						}else{
							$('button[name=register]').prop("disabled", false);
							$('input[name=login]').removeClass('exist');
						}
					});
			});

			$('button[name=register]').on('click',function() {
			    var list = getAllElements();
			    var monitor = false;
			    for(var i = 0; i < list.length; i++){
			        if(list[i].value == ''){
			            list[i].nextElementSibling.style.display = 'block';
			        }else{monitor = true;}
			    }
                if(monitor){
                    var password = $('input[name=password]').val();
                    var password2 = $('input[name=password2]').val();

                    if (!(password === password2)) {
                        $('input[name=password]').addClass('exist');
                        $('input[name=password2]').addClass('exist');
                    }
                    else{
                       $('form[class=add]').submit();
                    }
				}
    	});
    }

window.onload = function(){deleteRow();};