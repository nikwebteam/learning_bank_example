public class Main {

    /*������� ����������� �� ���� �����*/
    public static int minOfThree(int a, int b, int c) {
        if (a <= b && a <= c){
            return a;
        }else if (b <= a && b <= c){
            return b;
        }
        return  c;
    }

    /*��������� ��������� ��������� �����*/
    public static int factorial(int n) {
        int fact = 1, i = 1;
        while (i <= n) {
            fact *= i;
            i++;
        }
        return fact;
    }

    /*���������� ��� ������*/
    public static boolean compareString(String firstString, String secondString) {
        return firstString.equals(secondString);
    }

    /*��������� ���������� ������ � ����*/
    public static String concatenateString(String... strings) {
        String result = "";
        for (String s : strings)
            result += s;
        return result;
    }

    /*������� ������� � ���������� �������*/
    public static int findMinInArray(int[] array) {
        int min = array[0];
        for (int i = 0; i < array.length; i++) if (array[i] < min) min = array[i];
        return min;
    }

    /*��������� ������ �� �����������*/
    public static int[] sortArray(int[] array) {
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int buffer = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = buffer;
                }
            }
        }
        return array;
    }

    /*������� ����� ���������, ����������� ���� ������� ���������*/
    public static int sumOfElementsUnderMainDiagonal(int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++)
            for (int j = 0; j < matrix[0].length; j++) {
                if (j == i) break;
                sum += matrix[i][j];
            }
        return sum;
    }

    /*������� ���������� �����, ���������� ��������� ������ � �������*/
    public static int countPowerOfTwo(int[][] matrix) {
        int count = 0;
        int length = matrix[0].length;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < length; j++) {
                if ((matrix[i][j] & (matrix[i][j] - 1)) == 0){
                    count++;
                }
            }
        }
        return count;
    }

    /*����� ������ 1000 �����������*/
    public static void sleepFor1000() throws InterruptedException {
        Thread.sleep(1000);
    }

    /*����� ������� ����������*/
    public static void throwException() throws Exception {
        throw new Exception();
    }
}