import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Admin on 04.12.2015.
 */
public class MainTest {

    @Test
    public void testMinOfThree() throws Exception {
        Main main = new Main();
        int n = main.minOfThree(5,6,1);
        assertEquals(1,n);
        int n1 = main.minOfThree(5,1,6);
        assertEquals(1,n1);
        int n2 = main.minOfThree(1,6,5);
        assertEquals(1,n2);
        int n3 = main.minOfThree(5,1,1);
        assertEquals(1,n3);
        int n4 = main.minOfThree(1,1,6);
        assertEquals(1,n4);
        int n5 = main.minOfThree(1,6,1);
        assertEquals(1,n5);
        int n6 = main.minOfThree(1,1,1);
        assertEquals(1,n6);
        int n7 = main.minOfThree(5,6,-1);
        assertEquals(-1,n7);
    }

    @Test
    public void testFactorial() throws Exception {
        Main main = new Main();
        int n = main.factorial(5);
        assertEquals(120, n);
        int n1 = main.factorial(0);
        assertEquals(1, n1);
        int n2 = main.factorial(4);
        assertEquals(24, n2);
    }

    @Test
    public void testCompareString() throws Exception {
        Main main = new Main();
        Boolean n = main.compareString("name","name");
        assertEquals(true, n);
        Boolean n1 = main.compareString("name ","name");
        assertEquals(false, n1);
        Boolean n2 = main.compareString("nami","name");
        assertEquals(false, n2);
        Boolean n3 = main.compareString(""," ");
        assertEquals(false, n3);
        Boolean n4 = main.compareString("","");
        assertEquals(false, n4);
    }

    @Test
    public void testConcatenateString() throws Exception {
        Main main = new Main();
        String s = main.concatenateString("name","name");
        assertEquals("namename", s);
        String s1 = main.concatenateString("name ","name");
        assertEquals("name name", s1);
        String s2 = main.concatenateString("name"," ","name");
        assertEquals("name name", s2);
    }

    @Test
    public void testFindMinInArray() throws Exception {
        Main main = new Main();
        int[] arr = new int[]{1, 2, 3, 4, 5};
        int min = main.findMinInArray(arr);
        assertEquals(1, min);
        int[] arr1 = new int[]{2, 1, 3, 4, 5};
        int min1 = main.findMinInArray(arr1);
        assertEquals(1, min1);
        int[] arr2 = new int[]{3, 2, 1, 4, 5};
        int min2 = main.findMinInArray(arr2);
        assertEquals(1, min2);
        int[] arr3 = new int[]{1, 1, 3, 4, 5};
        int min3 = main.findMinInArray(arr3);
        assertEquals(1, min3);
        int[] arr4 = new int[]{1, 1, 1, 1, 1};
        int min4 = main.findMinInArray(arr4);
        assertEquals(1, min4);
        int[] arr5 = new int[]{5, 4, 3, 2, 1};
        int min5 = main.findMinInArray(arr5);
        assertEquals(1, min5);
    }

    @Test
    public void testSortArray() throws Exception {
        Main main = new Main();
        int[] arr = new int[]{3, 2, 1, 5, 4};
        int[] res = new int[5];
        res = main.sortArray(arr);
        assertEquals(1, res[0]);
        assertEquals(2, res[1]);
        assertEquals(3, res[2]);
        assertEquals(4, res[3]);
        assertEquals(5, res[4]);
    }

    @Test
    public void testSumOfElementsUnderMainDiagonal() throws Exception {
        Main main = new Main();
        int[][] arr = new int[][]{{1,1,1},{1,1,1},{1,1,1}};
        int n = main.sumOfElementsUnderMainDiagonal(arr);
        assertEquals(3, n);
        int[][] arr1 = new int[][]{{1,1,1,1},{1,1,1,1},{1,1,1,1},{1,1,1,1}};
        int n1 = main.sumOfElementsUnderMainDiagonal(arr1);
        assertEquals(6, n1);
        int[][] arr2 = new int[][]{{1,1},{1,1}};
        int n2 = main.sumOfElementsUnderMainDiagonal(arr2);
        assertEquals(1, n2);
    }

    @Test
    public void testCountPowerOfTwo() throws Exception {
        Main main = new Main();
        int[][] arr = new int[][]{{1,5,4},{3,6,12},{8,16,0}};
        int res = main.countPowerOfTwo(arr);
        assertEquals(4, res);
    }

    @Test
    public void testSleepFor1000() throws Exception {

    }

    @Test
    public void testThrowException() throws Exception {

    }
}