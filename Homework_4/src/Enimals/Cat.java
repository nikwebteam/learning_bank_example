package Enimals;

/**
 * Created by Admin on 05.12.2015.
 */
public class Cat extends Pet{
    String type = "Cat";

    @Override
    public void voice() {
        System.out.println("I am " + type + ". My name is " + name + ". My voice is myau!");
    }
}
