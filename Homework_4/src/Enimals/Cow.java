package Enimals;

/**
 * Created by Admin on 05.12.2015.
 */
public class Cow extends Pet{
    String type = "Cow";

    @Override
    public void voice() {
        System.out.println("I am " + type + ". My name is " + name + ". My voice is Muuu!");
    }
}
