package Human;

/**
 * Created by Admin on 05.12.2015.
 */
public class Man {
    protected String name = "";
    protected int age = 0;
    protected String gender = "";
    protected int weight = 0;

    public void setName(String name){
        this.name = name;
    }
    public void setAge(int age){
        this.age = age;
    }
    public void setWeight(int weight){
        this.weight = weight;
    }
}
