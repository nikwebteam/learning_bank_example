
public class Matrix {

    private int matrix[][];

    Matrix(int[][] arr){
        matrix = new int[arr.length][arr[0].length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                matrix[i][j] = arr[i][j];
            }
        }
    }

    public int[][] sumWithMatrix(int[][] anotherMatrix)throws IllegalArgumentException{
        int length = anotherMatrix[0].length;
        int[][] arr = new int[anotherMatrix.length][length];

        if (anotherMatrix.length == matrix.length && anotherMatrix[0].length == matrix[0].length) {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < length; j++) {
                    arr[i][j] = matrix[i][j];
                    arr[i][j] += anotherMatrix[i][j];
                }
            }
            return arr;
        }else {
             throw new IllegalArgumentException("Size your matrix bigger than size this.matrix") ;
        }
    }

    public int[][] multiplyByNumber(int number){
        int length = matrix[0].length;
        int[][] arr = new int[matrix.length][length];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < length; j++) {
                arr[i][j] = matrix[i][j];
                arr[i][j] *= number;
            }
        }
        return arr;
    }

    public int[][] multiplyByMatrix(int[][] anotherMatrix)throws IllegalArgumentException{

        int a = matrix.length;
        int b = matrix[0].length;
        int c = anotherMatrix.length;
        int d = anotherMatrix[0].length;

        if (b != c) {
            throw new IllegalArgumentException("...here could be your ad...");
        }
        int[][] result = new int[a][d];

        for (int i = 0; i < a; ++i)
            for (int j = 0; j < d; ++j)
                for (int e = 0; e < b; ++e) {
                    result[i][j] += matrix[i][e] * anotherMatrix[e][j];
                }
        return result;
    }

    public int[][] transposeMatrix(int[][] matrix)throws IllegalArgumentException{
       if(matrix.length == 0) {
            throw new IllegalArgumentException("Empty array");
        }
        int rowLength = matrix[0].length;
        for (int[] ai:matrix) {
            if (rowLength != ai.length) {
                throw new IllegalArgumentException("Non-equal rows");
            }
        }

        int [][] tMatrix = new int[rowLength][];
        for (int i = 0; i < rowLength; i++) {
            tMatrix[i] = new int[matrix.length];
        }
        for (int i = 0; i < matrix.length; i++) {
            int[] tArr = matrix[i];
            for (int j = 0; j < rowLength; j++) {
                tMatrix[j][i] = tArr[j];
            }
        }
        return tMatrix;
    }

    public void printMatrix(){
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
