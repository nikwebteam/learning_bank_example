import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Admin on 05.12.2015.
 */
public class MatrixTest {

    @Test
    public void testMatrix() throws Exception {
        int[][] arr = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        Matrix matrix = new Matrix(arr);
    }

    @Test
    public void testSumWithMatrix() throws Exception {
        int[][] arr = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        Matrix matrix = new Matrix(arr);
        matrix.sumWithMatrix(arr);
    }

    @Test
    public void testMultiplyByNumber() throws Exception {
        int[][] arr = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        Matrix matrix = new Matrix(arr);
        matrix.multiplyByNumber(1);
    }

    @Test
    public void testMultiplyByMatrix() throws Exception {
        int[][] arr = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12},{2,3,5,4}};
        Matrix matrix = new Matrix(arr);
        matrix.multiplyByMatrix(arr);
    }

    @Test
    public void testTransposeMatrix() throws Exception {
        int[][] arr = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        Matrix matrix = new Matrix(arr);
        matrix.transposeMatrix(arr);
    }

    @Test
    public void testPrintMatrix() throws Exception {
        int[][] arr = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        Matrix matrix = new Matrix(arr);
        matrix.printMatrix();
    }
}