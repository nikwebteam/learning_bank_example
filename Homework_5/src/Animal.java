
public abstract class Animal {

    protected String name;
    protected Integer countOfLegs;
    protected Integer countOfWings;
    protected String color;
    protected Type type;

    abstract void voice();

    enum Type{
        MAMMAL, AMPHIBIAN, BIRD, FISH;
    }
}
