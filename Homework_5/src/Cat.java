/**
 * Created by Admin on 10.12.2015.
 */
public class Cat extends Animal implements Runnable{

    Animal.Type type = Type.MAMMAL;

    public void voice(){
        System.out.println("My name is " + name + ". My color is " + color + ". I have " + countOfLegs + " legs" + ". I have " + countOfWings + " wings " + run() + " Who am I?");
    }

    Cat(String name, String color){
        this.name = name;
        this.color = color;
        this.countOfLegs = 4;
        this.countOfWings = 0;
    }

    @Override
    public String run() {
        return "I can Run!";
    }
}
