/**
 * Created by Admin on 10.12.2015.
 */
public class Run {
    public static void main(String[] args) {

        Sparrow sparrow = new Sparrow("Jack", "Brown");
        Lizard lizard = new Lizard("Nancy","Green");
        Salmon salmon = new Salmon("Nemo","Blue");
        Cat cat = new Cat("Tom","White");

        Animal[] arr = new Animal[]{sparrow, lizard, salmon, cat};

        for (int i = 0; i < arr.length; i++) {
            arr[i].voice();
        }
    }
}
