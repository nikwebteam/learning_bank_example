/**
 * Created by Admin on 10.12.2015.
 */
public class Salmon extends Animal implements Swimable{

    Animal.Type type = Type.FISH;

    public void voice(){
        System.out.println("My name is " + name + ". My color is " + color + ". I have " + countOfLegs + " legs" + ". I have " + countOfWings + " wings " + swim() + " Who am I?");
    }

    Salmon(String name, String color){
        this.name = name;
        this.color = color;
        this.countOfLegs = 0;
        this.countOfWings = 0;
    }

    @Override
    public String swim() {
        return "I can Swim!";
    }
}
