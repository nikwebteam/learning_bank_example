/**
 * Created by Admin on 10.12.2015.
 */
public class Sparrow extends Animal implements Flyable, Runnable{

    Animal.Type type = Type.BIRD;

    public void voice(){
        System.out.println("My name is " + name + ". My color is " + color + ". I have " + countOfLegs + " legs" + ". I have " + countOfWings + " wings " + fly() + " And " + run() + " Who am I?");
    }

    Sparrow(String name, String color){
        this.name = name;
        this.color = color;
        this.countOfLegs = 2;
        this.countOfWings = 2;
    }

    @Override
    public String fly() {
        return "I can Fly!";
    }

    @Override
    public String run(){
        return "I can Run!";
    }
}
