package Store_Of_Phone;

/**
 * Created by Admin on 11.12.2015.
 */
public class Run {
    public static void main(String[] args) {

        Shop shop = new Shop();

        Phone[] deliveryPhones = {new Phone("Nokia", "5300", 1, "Yes", "black", Phone.PhoneType.SLIDER),
                new Phone("Samsung", "B44", 2, "Yes", "white", Phone.PhoneType.SENSOR)};

        shop.newDelivery(deliveryPhones);

        shop.printPhonesInStock();

        Phone phone = new Phone("iPhone", "4S", 1, "Yes", "Gold", Phone.PhoneType.SENSOR);

        System.out.println("Count the same phone in stock is " + shop.countOfSearchPhone(phone));
    }
}
