package Store_Of_Phone;

/**
 * Created by Admin on 11.12.2015.
 */
public class Shop{

    Phone phone1 = new Phone("Nokia", "Lumia", 2, "Yes", "White", Phone.PhoneType.BLOCK);
    Phone phone2 = new Phone("Sony", "Z1", 1, "Yes", "Red", Phone.PhoneType.SLIDER);
    Phone phone3 = new Phone("Fly", "i520", 2, "Yes", "Black", Phone.PhoneType.FLIP);
    Phone phone4 = new Phone("iPhone", "4S", 1, "Yes", "Gold", Phone.PhoneType.SENSOR);

    Phone[] phonesInStock = new Phone[]{phone1, phone2, phone3, phone4};

    public Phone[] getPhonesInStock() {
        return phonesInStock;
    }

    public void newDelivery(Phone[] phonesDelivery){
        int thisLenght = phonesInStock.length;
        int newPonesArray = phonesDelivery.length;
        int sumLenght = thisLenght + newPonesArray;
        int count = 0;
        Phone[] arr = new Phone[sumLenght];
        for (int i = 0; i < sumLenght; i++) {
            if(i < thisLenght){
                arr[i] = phonesInStock[i];
            }else {
                arr[i] = phonesDelivery[count];
                count++;
            }
        }
        phonesInStock = arr;
    }

    public int countOfSearchPhone(Phone searchPhone){
        int result = 0;
        for (int i = 0; i < phonesInStock.length; i++) {
            if (phonesInStock[i].equals(searchPhone)){
                result++;
            }
        }
        return  result;
    }

    public void printPhonesInStock(){
        for (int i = 0; i < phonesInStock.length; i++) {
            System.out.println(phonesInStock[i].toString());
        }
    }
}
