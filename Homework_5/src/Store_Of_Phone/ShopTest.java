package Store_Of_Phone;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Admin on 11.12.2015.
 */
public class ShopTest {

    @Test
    public void testGetPhonesInStock() throws Exception {
        Shop shop = new Shop();
        shop.getPhonesInStock();
    }

    @Test
    public void testNewDelivery() throws Exception {
        Shop shop = new Shop();
        Phone[] deliveryPhones = {new Phone("Nokia", "5300", 1, "Yes", "black", Phone.PhoneType.SLIDER),
                new Phone("Samsung", "B44", 2, "Yes", "white", Phone.PhoneType.SENSOR)};
        shop.newDelivery(deliveryPhones);

        shop.printPhonesInStock();
    }

    @Test
    public void testCountOfSearchPhone() throws Exception {
        Shop shop = new Shop();
        Phone phone = new Phone("iPhone", "4S", 1, "Yes", "Gold", Phone.PhoneType.SENSOR);
        int res = shop.countOfSearchPhone(phone);
        assertEquals(1, res);
    }

    @Test
    public void testPrintPhonesInStock() throws Exception {
        Shop shop = new Shop();
        shop.printPhonesInStock();
    }
}