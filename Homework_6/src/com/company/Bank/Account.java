package com.company.Bank;

public class Account implements Comparable{
    private Long accountNumber;
    private Double balance;
    public static Long GLOBAL_ACCOUNT_NUMBER = 1_000_000L;

    public Account(Long accountNumber, Double balance) {
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public Double getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        return accountNumber.equals(account.accountNumber);

    }

    @Override
    public int hashCode() {
        return accountNumber.hashCode();
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber=" + accountNumber +
                ", balance=" + balance +
                '}';
    }
    public void increaseBalance(Double sum){
        balance += sum;
    }
    public void decreaseBalance(Double sum){
        balance -= sum;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
