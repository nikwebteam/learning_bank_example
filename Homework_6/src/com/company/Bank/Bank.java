package com.company.Bank;

import com.company.Bank.Exceptions.AccountNotExistException;
import com.company.Bank.Exceptions.ClientAlreadyExistException;
import com.company.Bank.Exceptions.NonPositiveAmountException;
import com.company.Bank.Exceptions.NotEnoughBalanceException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Bank {
    private List<Client> clients = new ArrayList<>();
    private List<Account> accounts = new ArrayList<>();
    private List<Transaction> transactions = new ArrayList<>();

    public List<Client> getClients() {
        return clients;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }
//===========================================================================================//
    public void addAccount(Client client){
        Account.GLOBAL_ACCOUNT_NUMBER++;
        accounts.add(new Account(Account.GLOBAL_ACCOUNT_NUMBER, 0.0));
        client.addAccount(new Account(Account.GLOBAL_ACCOUNT_NUMBER, 0.0));
    }
//====================================================================================//
    public void addClients(Client client) throws ClientAlreadyExistException {
        if (findClient(client) != null) {
            throw new ClientAlreadyExistException("Sorry but this client already exist :(");
        }
        clients.add(client);
        addAccount(client);
    }
//===================================================================================//
    public Client findClient(Client searchClient){
        for (Client client : clients) {
            if (searchClient.equals(client)) {
                return client;
            }
        }
        return null;
    }
//==================================================================================//
    public Account findAccount(Long searchAccountNumber){
        for (Account account : accounts) {
            if (searchAccountNumber.equals(account.getAccountNumber())) {
                return account;
            }
        }
        return null;
    }
//==============================================================================================================================//
    public void putIntoAccount(Long accountNumber, Double amount) throws NotEnoughBalanceException, AccountNotExistException {

        Account foundAccount = findAccount(accountNumber);

        if (amount <= 0D)
        {
            throw new NotEnoughBalanceException("Your amount must be bigger than zero");
        }
        else if (foundAccount == null)
        {
            throw new AccountNotExistException("Your account not found");
        }
        foundAccount.increaseBalance(amount);
        transactions.add(new Transaction(foundAccount, Operation.PUT, amount));
    }
//===============================================================================================================//
    public void withdrawFromAccount(Long accountNumber, Double amount) throws NonPositiveAmountException,
            NotEnoughBalanceException, AccountNotExistException {

        Account account = findAccount(accountNumber);

        if (amount <= 0) {
            throw new NonPositiveAmountException("Your amount must be bigger than zero");
        }
        else if (account.getBalance() < amount) {
            throw new NotEnoughBalanceException("You can't withdraw the bigger amount than you have");
        }
        else if (account == null) {
           throw new AccountNotExistException("Your account not found");
        }
        account.decreaseBalance(amount);
        transactions.add(new Transaction(account, Operation.WITHDRAW, amount));
    }
//====================================================================================================================//
    public void moveFromOneAccountToAnother(Long sourceAccountNumber, Long destinationAccountNumber, Double amount)
            throws NotEnoughBalanceException, NonPositiveAmountException, AccountNotExistException {

        Account sourceAccount = findAccount(sourceAccountNumber);
        Account destinationAccount = findAccount(destinationAccountNumber);

        if (amount < 0)
        {
            throw new NonPositiveAmountException("Your amount must be bigger than zero");
        }
        else if (sourceAccount == null){
            throw new AccountNotExistException("Source account not found");
        }
        else if (sourceAccount.getBalance() < amount)
        {
            throw new NotEnoughBalanceException("You can't withdraw the bigger amount than you have");
        }

        sourceAccount.decreaseBalance(amount);

        if (destinationAccount == null) {
            sourceAccount.increaseBalance(amount);
            throw new AccountNotExistException("Destination account not found");
        }
        else
        {
            putIntoAccount(destinationAccountNumber, amount);
            transactions.add(new Transaction(sourceAccount,Operation.WITHDRAW,amount));
        }
    }
//============================================================================================================//
    public Account findAccountsByClient(Client searchClient){
        for (Account account : searchClient.getAccounts()) {
            return account;
        }
        return null;
    }
//=======================================================================//
    public void printClients(){
        for (Client client : clients) {
            System.out.println(client);
        }
    }
//========================================================================//
    public void printAccounts(){
        Collections.sort(accounts);
        for (Account account : accounts) {
            System.out.println(account);
        }
    }
//=======================================================================//
    public void printAccountsForClient(Client client){
        for (Account account : client.getAccounts()) {
            System.out.println(account);
        }
    }
//========================================================================================//
    public List<Transaction> findTransactionByAccount(Long searchAccountNumber){
        List<Transaction> result = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (searchAccountNumber.equals(transaction.getSource().getAccountNumber())){
                result.add(transaction);
            }
        }
        return result;
    }
//=============================================================================================//
    public List<Transaction> sortTransactionsFirstPut(List<Transaction> transaction){

        List<Transaction> res = new ArrayList<>();
        for (Transaction transaction1 : transactions) {
            if (transaction1.getOperation().equals(Operation.PUT)){
                res.add(transaction1);
            }
        }

        for (Transaction transaction1 : transactions) {
            if (transaction1.getOperation().equals(Operation.WITHDRAW)){
                res.add(transaction1);
            }
        }
        return res;
    }
//==================================================================================================//
    public List<Transaction> sortTransactionsFirstWithdraw(List<Transaction> transaction){

        List<Transaction> res = new ArrayList<>();
        for (Transaction transaction1 : transactions) {
            if (transaction1.getOperation().equals(Operation.WITHDRAW)){
                res.add(transaction1);
            }
        }

        for (Transaction transaction1 : transactions) {
            if (transaction1.getOperation().equals(Operation.PUT)){
                res.add(transaction1);
            }
        }
        return res;
    }
//=======================================================================================================//
    public void printTransactionsForAccount(Long accountNumber){
        for (Transaction transaction : transactions) {
            if (accountNumber.equals(transaction.getSource().getAccountNumber())){
                System.out.println(transaction);
            }
        }
    }
}
//===============================---END---=============================================================//