package com.company.Bank;

import org.junit.Before;
import org.junit.Test;
import java.sql.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BankTest {

    @Before
    public void init() {
        Account.GLOBAL_ACCOUNT_NUMBER = 1000000L;
    }

    @Test
    public void testAddAccount() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Bank bank = new Bank();
        bank.addClients(client);
      // assertEquals(1, bank.getAccounts().length);
        System.out.println(bank.getClients());
    }

    @Test
    public void testAddClients() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Bank bank = new Bank();
        bank.addClients(client);
        assertEquals(1, bank.getClients().size());
    }

    @Test
    public void testFindClient() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Bank bank = new Bank();
        bank.addClients(client);
        assertEquals(client, bank.findClient(client));
    }

    @Test
    public void testFindAccount() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Bank bank = new Bank();
        bank.addClients(client);
        assertEquals(bank.getAccounts().get(0), bank.findAccount(1_000_001L));
    }

    @Test
    public void testPutIntoAccount() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Bank bank = new Bank();
        bank.addClients(client);
        bank.putIntoAccount(1_000_001L, 2.0);
        Account new1 = bank.findAccount(1_000_001L);
        Double s = 2.0;
        assertEquals(s, new1.getBalance());
    }

    @Test
    public void testWithdrawFromAccount() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Bank bank = new Bank();
        bank.addClients(client);
        bank.putIntoAccount(1_000_001L, 2.0);
        bank.withdrawFromAccount(1_000_001L, 1.0);
        Account new1 = bank.findAccount(1_000_001L);
        Double s = 1.0;
        assertEquals(s, new1.getBalance());
    }

    @Test
    public void testMoveFromOneAccountToAnother() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Client client2 = new Client("Nilson", "Makarof", Date.valueOf("1992-05-07"), "Kyiv", "+380659874258");
        Bank bank = new Bank();
        bank.addClients(client);
        bank.addClients(client2);
        bank.putIntoAccount(1_000_001L, 2.0);
        bank.putIntoAccount(1_000_002L, 2.0);
        bank.moveFromOneAccountToAnother(1_000_001L, 1_000_002L, 1.0);
        Account new1 = bank.findAccount(1_000_001L);
        Account new2 = bank.findAccount(1_000_002L);
        if (new1.getBalance() == 1.0 && new2.getBalance() == 3.0){
            System.out.println("true");
        }else{
            System.out.println("false");
        }
    }

    @Test
    public void testFindAccountsByClient() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Bank bank = new Bank();
        bank.addClients(client);
        assertEquals(bank.getAccounts().get(0), bank.findAccountsByClient(client));
    }

    @Test
    public void testPrintClients() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Client client2 = new Client("Nilson", "Makarof", Date.valueOf("1992-05-07"), "Kyiv", "+380659874258");
        Bank bank = new Bank();
        bank.addClients(client);
        bank.addClients(client2);
        bank.printClients();
    }

    @Test
    public void testPrintAccounts() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Client client2 = new Client("Nilson", "Makarof", Date.valueOf("1992-05-07"), "Kyiv", "+380659874258");
        Bank bank = new Bank();
        bank.addClients(client);
        bank.addClients(client2);
        bank.printAccounts();
    }

    @Test
    public void testPrintAccountsForClient() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Client client2 = new Client("Nilson", "Makarof", Date.valueOf("1992-05-07"), "Kyiv", "+380659874258");
        Bank bank = new Bank();
        bank.addClients(client);
        bank.addClients(client2);
        bank.printAccountsForClient(client);
    }

    @Test
    public void testfindTransactionByAccount() throws Exception {
        Client client = new Client("Nikl", "Chupikovn", Date.valueOf("1991-05-18"), "Dnepropegrovsk", "+380659872562");
        Bank bank = new Bank();
        bank.addClients(client);
        bank.putIntoAccount(1_000_001L, 2.0);
        System.out.println(bank.getTransactions().get(0).toString());
        List<Transaction> tmp = bank.findTransactionByAccount(1_000_001L);
        Long tmp2 = bank.findAccount(1_000_001L).getAccountNumber();
        assertEquals(tmp2, tmp.get(0).getSource().getAccountNumber());
    }

    @Test
    public void testSortTransactionsFirstPut() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Client client2 = new Client("Nilson", "Makarof", Date.valueOf("1992-05-07"), "Kyiv", "+380659874258");
        Bank bank = new Bank();
        bank.addClients(client);
        bank.addClients(client2);
        bank.putIntoAccount(1_000_001L, 2.0);
        bank.withdrawFromAccount(1_000_001L, 1.0);
        bank.putIntoAccount(1_000_002L, 2.0);
        bank.withdrawFromAccount(1_000_002L, 1.0);

        // ����� �������� ���� ����
    }

    @Test
    public void testSortTransactionsFirstWithdraw() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Client client2 = new Client("Nilson", "Makarof", Date.valueOf("1992-05-07"), "Kyiv", "+380659874258");
        Bank bank = new Bank();
        bank.addClients(client);
        bank.addClients(client2);
        bank.putIntoAccount(1_000_001L, 2.0);
        bank.withdrawFromAccount(1_000_001L, 1.0);
        bank.putIntoAccount(1_000_002L, 2.0);
        bank.withdrawFromAccount(1_000_002L, 1.0);
        // ����� �������� ���� ����
    }

    @Test
    public void testPrintTransactions() throws Exception {
        Client client = new Client("Nik", "Chupikov", Date.valueOf("1991-05-08"), "Dnepropetrovsk", "+380659874562");
        Client client2 = new Client("Nilson", "Makarof", Date.valueOf("1992-05-07"), "Kyiv", "+380659874258");
        Bank bank = new Bank();
        bank.addClients(client);
        bank.addClients(client2);
        bank.putIntoAccount(1_000_001L, 2.0);
        bank.withdrawFromAccount(1_000_001L, 1.0);
        bank.putIntoAccount(1_000_002L, 2.0);
        bank.withdrawFromAccount(1_000_002L, 1.0);
        bank.printTransactionsForAccount(bank.getAccounts().get(0).getAccountNumber());
    }
}