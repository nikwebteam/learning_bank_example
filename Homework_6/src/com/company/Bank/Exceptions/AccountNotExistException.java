package com.company.Bank.Exceptions;

public class AccountNotExistException extends Exception{

    public AccountNotExistException(String message){
        super(message);
    }

    public AccountNotExistException(String message, Throwable cause){
        super(message, cause);
    }

    public AccountNotExistException(Throwable cause){
        super(cause);
    }
}
