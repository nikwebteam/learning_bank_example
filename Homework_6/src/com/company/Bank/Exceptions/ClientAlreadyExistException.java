package com.company.Bank.Exceptions;

public class ClientAlreadyExistException extends Exception{

    public ClientAlreadyExistException(String message){
        super(message);
    }

    public ClientAlreadyExistException(String message, Throwable cause){
        super(message, cause);
    }

    public ClientAlreadyExistException(Throwable cause){
        super(cause);
    }
}
