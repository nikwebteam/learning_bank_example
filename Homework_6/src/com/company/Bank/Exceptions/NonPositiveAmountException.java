package com.company.Bank.Exceptions;

public class NonPositiveAmountException extends Exception{

    public NonPositiveAmountException(String message){
        super(message);
    }

    public NonPositiveAmountException(String message, Throwable cause){
        super(message, cause);
    }

    public NonPositiveAmountException(Throwable cause){
        super(cause);
    }
}
