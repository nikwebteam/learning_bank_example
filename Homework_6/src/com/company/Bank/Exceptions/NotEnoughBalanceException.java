package com.company.Bank.Exceptions;

/**
 * Created by java on 16.12.2015.
 */
public class NotEnoughBalanceException extends Exception {

    public NotEnoughBalanceException(String message){
        super(message);
    }

    public NotEnoughBalanceException(String message, Throwable cause){
        super(message, cause);
    }

    public NotEnoughBalanceException(Throwable cause){
        super(cause);
    }
}
