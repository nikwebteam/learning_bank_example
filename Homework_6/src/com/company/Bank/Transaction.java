package com.company.Bank;

/**
 * Created by Admin on 12.12.2015.
 */
enum Operation{PUT, WITHDRAW}

public class Transaction {
    private Account source;
    private Operation operation;
    private Double amount;

    public Account getSource() {
        return source;
    }

    public Operation getOperation() {
        return operation;
    }

    public Double getAmount() {
        return amount;
    }

    public Transaction(Account source, Operation operation, Double amount) {
        this.source = source;
        this.operation = operation;
        this.amount = amount;
    }

/*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        if (!source.equals(that.source)) return false;
        if (operation != that.operation) return false;
        return amount.equals(that.amount);

    }

    @Override
    public int hashCode() {
        int result = source.hashCode();
        result = 31 * result + operation.hashCode();
        result = 31 * result + amount.hashCode();
        return result;
    }
*/

    @Override
    public String toString() {
        return "Transaction{" +
                "source=" + source.getAccountNumber() +
                ", operation=" + operation +
                ", amount=" + amount +
                '}';
    }
}
