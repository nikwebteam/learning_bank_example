package TaskAboutSport;

public class Football extends Discipline{

    public Football(String name,String description){
        this.name = name;
        this.description = "It's a " + name + ". " + description;
    }
}
