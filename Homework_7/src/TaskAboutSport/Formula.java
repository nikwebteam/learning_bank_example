package TaskAboutSport;

public class Formula extends Discipline{

    public Formula(String name, String description){
        this.name = name;
        this.description = "It's a " + name + ". " + description;
    }
}
