package TaskAboutSport;

public class Hockey extends Discipline{

    public Hockey(String name, String description){
        this.name = name;
        this.description = "It's a " + name + ". " + description;
    }
}
