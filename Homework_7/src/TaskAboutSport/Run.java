package TaskAboutSport;

import TaskAboutSport.AllForSports.Ball;
import TaskAboutSport.AllForSports.Car;
import TaskAboutSport.AllForSports.Fans;
import TaskAboutSport.AllForSports.Ice;

import java.util.ArrayList;

public class Run {
    public static void main(String[] args) {
        Football football = new Football("Football", "Its a game for my soul and I like it very much.");
        Hockey hockey = new Hockey("Hockey", "Its a game for the real man who don't afraid a cold.");
        Formula formula = new Formula("Formula", "Its a very fast drive in a car and very cool sport.");

        Fans fans = new Fans("Just fans!");
        Ball ball = new Ball("Just a bAll!");
        Car car = new Car("Just a car!");
        Ice ice = new Ice("Just a ice!");

       /* Sport<Football, Ball> footballBallSport = new Sport<>(football, ball);
        Sport<Hockey, Ice> hockeyIceSport = new Sport<>(hockey, ice);
        Sport<Formula, Car> formulaCarSport = new Sport<>(formula, car);
        Sport<Football, Fans> footballFansSport = new Sport<>(football, fans);

        footballBallSport.cantLiveWithout();
        hockeyIceSport.cantLiveWithout();
        formulaCarSport.cantLiveWithout();
        footballFansSport.cantLiveWithout();*/
        
        ArrayList<Constituent> footballConstituents = new ArrayList<>();
        footballConstituents.add(ball);
        footballConstituents.add(fans);
        
        Sport<Football, Constituent> footballPassion = new Sport<>(football, footballConstituents);
        
        footballPassion.cantLiveWithout2();
        
    }
}
