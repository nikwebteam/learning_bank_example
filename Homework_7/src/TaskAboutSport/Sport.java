package TaskAboutSport;

import java.util.ArrayList;

public class Sport <K extends Discipline, V extends Constituent> {

    private K discipline;
   // private V constituent;
    private ArrayList<V> constituents;

    public Sport(K discipline, ArrayList<V> constituent) {
        this.discipline = discipline;
        this.constituents = constituent;
    }

   /* public void cantLiveWithout(){
        System.out.println(discipline.getDescription() + " I can't live without " + constituent.getAbout());
    }*/
    public void cantLiveWithout2(){
        System.out.print(discipline.getDescription() + " I can't live without ");
        for (int i = 0; i < constituents.size(); i++){
            System.out.print(constituents.get(i).getAbout());
        }
    }
}
