package TaskList;

public class Element<T> {

    private Element<T> prev;
    private T value;
    private Element<T> next;

    public Element(T value){
        this.value = value;
    }

//============ Methods get ====================//
    public T getValue(){return value;}

    public boolean hasNext(){return next != null;}

    public Element<T> getNext(){return next;}

    public boolean hasPrev(){
    return prev != null;
    }

    public Element<T> getPrev(){return prev;}
//======================================================================//
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Element<?> element = (Element<?>) o;

        return value.equals(element.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
//=========================================================================//

    public void addAfter(Element<T> element){
        next = element;
    }

    public void addBefore(Element<T> element){
        prev = element;
    }
}
