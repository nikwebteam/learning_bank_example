package TaskList;

import java.util.List;

public class LinkedList<T> {
    private Element<T> first;

    public boolean isEmpty(){
        return first == null;
    }

    public Element<T> getFirst(){return first;}

    public void add(Element<T> element){
        if (isEmpty()){
            first = element;
        }else if (!(isEmpty()) && !(getFirst().hasNext())){
            element.addBefore(getFirst());
            getFirst().addAfter(element);
        }else if (getFirst().hasNext()){
            Element tmp = getFirst().getNext();
            while (tmp.hasNext()){
                Element a = tmp.getNext();
                tmp = a;
            }
            element.addBefore(tmp);
            tmp.addAfter(element);
        }
    }

    public void remove(Element<T> element){
        if (isEmpty()){
        }
        else if (!(getFirst().hasNext())){
            if (getFirst().equals(element)) {
                first = null;
            }
        }else {
            Element elem = getFirst();
            while (elem.hasNext()) {
                if (elem.equals(element) && elem.equals(getFirst())){
                    Element asd = getFirst().getNext();
                    asd.addBefore(null);
                    first = asd;
                    return;
                }else if (elem.equals(element)) {
                    Element pr = elem.getPrev();
                    Element nex = elem.getNext();
                    elem = null;
                    pr.addAfter(nex);
                    nex.addBefore(pr);
                    return;
                } else {
                    Element newElement = elem.getNext();
                    elem = newElement;
                }
            }
            if (elem.equals(element)){
                Element pr = elem.getPrev();
                elem = null;
                pr.addAfter(null);
            }
        }
    }

    public boolean contains(Element<T> element){
        if (isEmpty()){
            return false;
        }else if (getFirst().equals(element)){
            return true;
        }else if (!(getFirst().hasNext())){
            return false;
        }else {
            Element tmpElement = getFirst().getNext();
            while (tmpElement.hasNext()) {
                if (tmpElement.equals(element)) {
                    return true;
                } else {
                    Element tmp = tmpElement.getNext();
                    tmpElement = tmp;
                }
            }
            if (tmpElement.equals(element)){
                return true;
            }
        }
        return false;
    }

    public int count(){
        int count = 1;
        if (isEmpty()) {
            return 0;
        }
        else if( !(isEmpty())  && !(getFirst().hasNext()) )
            return 1;
        else {
            Element tmp = getFirst();
            while (tmp.hasNext()){
                Element a = tmp.getNext();
                tmp = a;
                count++;
            }
        }
        return count;
    }

    public void addAfter(Element<T> element, Element<T> newElem){
        if (isEmpty()){
        }
        else if (!(getFirst().hasNext())){
            if (getFirst().equals(element)) {
                getFirst().addAfter(newElem);
                newElem.addBefore(getFirst());
            }
        }else {
            Element tmp = getFirst();
            while (tmp.hasNext()){
                if (tmp.equals(element)){
                    newElem.addAfter(tmp.getNext());
                    newElem.addBefore(tmp);
                    tmp.getNext().addBefore(newElem);
                    tmp.addAfter(newElem);
                    return;
                }else {
                    Element a = tmp.getNext();
                    tmp = a;
                }
            }
            if (tmp.equals(element)){
                tmp.addAfter(newElem);
                newElem.addBefore(tmp);
            }
        }
    }

    public void addBefore(Element<T> elem, Element<T> newElem){
        if(isEmpty()){
        }else if (!(getFirst().hasNext())){
            if (getFirst().equals(elem)){
                newElem.addAfter(getFirst());
                getFirst().addBefore(newElem);
                first = newElem;
            }
        }else {
            Element tmp = getFirst();
            while (tmp.hasNext()){
                if (tmp.equals(elem) && tmp.equals(getFirst())){
                    newElem.addAfter(getFirst());
                    getFirst().addBefore(newElem);
                    first = newElem;
                    return;
                }
                else if (tmp.equals(elem)){
                    newElem.addBefore(tmp.getPrev());
                    tmp.getPrev().addAfter(newElem);
                    newElem.addAfter(tmp);
                    tmp.addBefore(newElem);
                    return;
                }else {
                    Element e = tmp.getNext();
                    tmp = e;
                }
            }
            if (tmp.equals(elem)){
                newElem.addBefore(tmp.getPrev());
                tmp.getPrev().addAfter(newElem);
                newElem.addAfter(tmp);
                tmp.addBefore(newElem);
            }
        }
    }

    public void addAll(List<Element> list){
        for (Element e : list){
            add(e);
        }
    }
}
