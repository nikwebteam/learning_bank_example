package TaskList;

import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        // Test 1
        LinkedList<String> list = new LinkedList<>();
        Assert.assertTrue(list.isEmpty());

        // Test 2
        Element<String> element1 = new Element<>("Nik");
        // Test 2.1
        list.add(element1);
        Assert.assertFalse(list.isEmpty());
        // Test 2.2
        Assert.assertTrue(list.contains(element1));
        // Test 2.3
        Assert.assertEquals(element1, list.getFirst());

        // Test 3
        LinkedList<Integer> intList = new LinkedList<>();
        Element<Integer> ele1 = new Element<>(1);
        Element<Integer> ele2 = new Element<>(2);
        Element<Integer> ele3 = new Element<>(3);
        intList.add(ele1);
        intList.add(ele2);
        intList.add(ele3);
        // Test 3.1
        Assert.assertEquals(3, intList.count());
        // Test 3.2
        Assert.assertTrue(intList.contains(ele1));
        Assert.assertTrue(intList.contains(ele2));
        Assert.assertTrue(intList.contains(ele3));

        // Test 4
        LinkedList<Integer> intList2 = new LinkedList<>();
        Element<Integer> ele01 = new Element<>(1);
        Element<Integer> ele02 = new Element<>(2);
        Element<Integer> ele03 = new Element<>(3);
        intList2.add(ele01);
        intList2.add(ele02);
        intList2.add(ele03);
        intList2.remove(ele02);
        Assert.assertEquals(intList2.getFirst().getNext(), ele03);

        //Test 5
        LinkedList<Integer> intList3 = new LinkedList<>();
        Element<Integer> elem1 = new Element<>(1);
        Element<Integer> elem2 = new Element<>(2);
        Element<Integer> elem3 = new Element<>(3);
        intList3.add(elem1);
        intList3.add(elem2);
        intList3.add(elem3);
        // 5.1
        Assert.assertTrue(intList3.getFirst().getPrev() == null);
        Assert.assertFalse(intList3.getFirst().getNext() == null);
        Assert.assertEquals(intList3.getFirst().getNext(), elem2);
        // 5.2
        Assert.assertTrue(intList3.getFirst().getNext().getNext().getNext() == null);
        Assert.assertFalse(intList3.getFirst().getNext().getNext().getPrev() == null);
        Assert.assertEquals(intList3.getFirst().getNext().getNext().getPrev(), elem2);
        // 5.3
        Assert.assertEquals(elem1, intList3.getFirst().getNext().getPrev());
        Assert.assertEquals(elem3, intList3.getFirst().getNext().getNext());

        // Test 6 (addBefore)
        Element element = new Element(4);
        intList3.addBefore(elem1, element);
        Assert.assertEquals(element, intList3.getFirst());
        Assert.assertTrue(intList3.getFirst().getPrev() == null);
        Assert.assertEquals(elem1, intList3.getFirst().getNext());

        Element preLast = new Element(5);
        intList3.addBefore(elem3, preLast);
        Assert.assertEquals(preLast, intList3.getFirst().getNext().getNext().getNext());
        Assert.assertEquals(elem2, intList3.getFirst().getNext().getNext().getNext().getPrev());
        Assert.assertEquals(elem3, intList3.getFirst().getNext().getNext().getNext().getNext());

        // Test 6 (addAfter)
        LinkedList<String> newList = new LinkedList<>();
        Element one = new Element("one");
        Element two = new Element("two");
        Element three = new Element("three");
        newList.add(one);
        newList.add(two);
        newList.add(three);

        Element four = new Element("four");
        newList.addAfter(one, four);
        Assert.assertEquals(four, newList.getFirst().getNext());
        Assert.assertEquals(four, newList.getFirst().getNext().getNext().getPrev());
        Assert.assertEquals(newList.getFirst().getNext().getPrev(), newList.getFirst());
        Assert.assertEquals(newList.getFirst().getNext().getNext(), two);

        // Test 7 (addAll)
        LinkedList<String> newList1 = new LinkedList<>();
        Element one1 = new Element("one1");
        Element two1 = new Element("two1");
        Element three1 = new Element("three1");
        newList1.add(one1);
        newList1.add(two1);
        newList1.add(three1);

        Assert.assertEquals(3, newList1.count());

        List<Element> arr = new ArrayList<>();
        Element one2 = new Element("one2");
        Element two2 = new Element("two2");
        Element three2 = new Element("three2");
        arr.add(one2);
        arr.add(two2);
        arr.add(three2);

        newList1.addAll(arr);

        Assert.assertEquals(6, newList1.count());

    }
}
