package TaskWithAnimal;

public class ZooBox<T extends Animal> {
    private T animal;

    public void lockAnimal(T animal){
        this.animal = animal;
    }

    public T getAnimal(){
        return animal;
    }
}
