package TaskWithAnimal.allAnimal;

import TaskWithAnimal.Animal;
import TaskWithAnimal.AnimalType;

public class Monkey extends Animal {

    public Monkey(String name){
        this.name = name;
        this.animalType = AnimalType.MAMMAL;
    }
    @Override
    public void sayName() {
        System.out.println("I'm a " + this.animalType + ". My name is " + this.name);
    }
}
