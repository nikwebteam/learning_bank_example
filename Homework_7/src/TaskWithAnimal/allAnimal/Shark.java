package TaskWithAnimal.allAnimal;

import TaskWithAnimal.Animal;
import TaskWithAnimal.AnimalType;

public class Shark extends Animal {

    public Shark(String name){
        this.name = name;
        this.animalType = AnimalType.FISH;
    }
    @Override
    public void sayName() {
        System.out.println("I'm a " + this.animalType + ". My name is " + this.name);
    }
}
