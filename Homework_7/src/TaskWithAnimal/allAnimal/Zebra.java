package TaskWithAnimal.allAnimal;

import TaskWithAnimal.Animal;
import TaskWithAnimal.AnimalType;

public class Zebra extends Animal {

    public Zebra(String name){
        this.name = name;
        this.animalType = AnimalType.MAMMAL;
    }
    @Override
    public void sayName() {
        System.out.println("I'm a " + this.animalType + ". My name is " + this.name);
    }
}
