package Collections_Practice;

import java.util.HashMap;
import java.util.Map;

public class CountOfLetter {
    public static void main(String[] args) {
        countOfLetter("ddddgggiiiyyyyyjjjjddd");
    }

    public static void countOfLetter(String string){
        int length = string.length();
        Map<Character, Integer> map = new HashMap<>();
        int count = 0;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                if (string.charAt(i) == string.charAt(j)){
                    count++;
                }
            }
            map.put(string.charAt(i), count);
            count = 0;
        }
        /*for (Map.Entry<Character,Integer> m : map.entrySet()){
            System.out.println(m.getKey() + " : " + m.getValue());
        }*/
    }

}
