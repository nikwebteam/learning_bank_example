package Collections_Practice;

import java.util.LinkedList;
import java.util.Queue;

public class Countdown {
    public static void main(String[] args) throws InterruptedException {

        Queue<Integer> queue = new LinkedList<Integer>();
        for (int i = 10; i > 0; i--) {
            queue.offer(i);
        }
        while (!queue.isEmpty()){
            System.out.print(queue.poll() + " ");
            Thread.sleep(1000);
        }
    }
}