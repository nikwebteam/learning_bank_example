package Collections_Practice;

import java.util.ArrayList;
import java.util.List;

public class ListDiv2And3 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 6; i++) {
            list.add(i);
        }
        System.out.println(change(list));
    }

    public static List<Integer> change(List<Integer> list){
        List<Integer> newList = new ArrayList<>(list);

        for (int i = 0; i < list.size(); i++){
            if (list.get(i) %2 == 0 && list.get(i) %3 == 0){

            }else if (list.get(i) %3 == 0){
                newList.add(i,list.get(i));
            }else if (list.get(i) %2 == 0){
                newList.remove(i);
            }
        }
        return newList;
    }
}