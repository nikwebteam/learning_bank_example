package Collections_Practice;

import java.util.HashMap;
import java.util.Map;

public class RemoveWithEqualValues {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("nik",2);
        map.put("nik1",3);
        map.put("nik2",1);
        map.put("nik3",4);
        map.put("nik4",2);
        map.put("nik5",1);
        map.put("nik6",2);
        map.put("nik7",1);
        //removeNotFirstEqualValues(map);
        //removeItemFromMapByValue(map, 4);
        //removeEqualValues(map);
        //removeItemFromMapByKey(map, "nik");
        for (Map.Entry<String, Integer> m : map.entrySet()){
            System.out.println(m.getKey() + " : " + m.getValue());
        }

    }

    public static <K, V> void removeItemFromMapByValue(Map<K, V> map, V value){
        Map<K, V> copy = new HashMap<>(map);
        for (Map.Entry<K, V> m : copy.entrySet()){
            if(m.getValue().equals(value)){
                map.remove(m.getKey());
            }
        }
    }
    public static <K, V> void removeEqualValues(Map<K, V> map){
        Map<K, V> copy = new HashMap<>(map);
        Map<K, V> copy2 = new HashMap<>(map);

        for (Map.Entry<K, V> m : copy.entrySet())
        {
            copy2.remove(m.getKey());

            if (copy2.containsValue(m.getValue()))
            {
                removeItemFromMapByValue(map, m.getValue());
            }
        }
    }
    public static <K, V> void removeItemFromMapByKey(Map<K, V> map, K key){
        map.remove(key);
    }
    public static <K, V> void removeNotFirstEqualValues(Map<K, V> map){
        Map<K, V> copy = new HashMap<>(map);
        Map<K, V> copy2 = new HashMap<>(map);
        int count = 0;
        for (Map.Entry<K, V> m : copy.entrySet()) {
            for (Map.Entry<K, V> x : copy2.entrySet()){
                if (m.getValue().equals(x.getValue())){
                    if (count != 1){
                        count = 1;
                    }else {
                        removeItemFromMapByKey(map, x.getKey());
                    }
                }
            }
            count = 0;
        }
    }
}
