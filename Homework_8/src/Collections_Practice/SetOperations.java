package Collections_Practice;

import java.util.HashSet;
import java.util.Set;

public class SetOperations {
    public static void main(String[] args) {
        Set<Integer> a1 = new HashSet<>();
        a1.add(5);
        a1.add(4);
        a1.add(3);
        Set<Integer> a2 = new HashSet<>();
        a2.add(6);
        a2.add(4);
        a2.add(2);

        System.out.println(isSubset(a1,a2));
        System.out.println(union(a1,a2));
        System.out.println(intersection(a1,a2));
        System.out.println(difference(a1,a2));
        System.out.println(symDifference(a1,a2));
    }

    public static <T> boolean isSubset(Set<T> a, Set<T> b){
        for (T e : b){
            if ( !(a.contains(e)) ){
                return false;
            }
        }
        return true;
    }

    public static <T> Set union(Set<T> a, Set<T> b){
        Set<T> newSet = new HashSet<>();
        newSet.addAll(a);
        newSet.addAll(b);
        return newSet;
    }

    public static <T> Set intersection(Set<T> a, Set<T> b){
        Set<T> newSet = new HashSet<>();
        for (T e : a){
            if (a.contains(e)){
                newSet.add(e);
            }
        }
        return newSet;
    }

    public static <T> Set difference(Set<T> a, Set<T> b){
        Set<T> newSet = new HashSet<>();
        for (T e : a){
            if (!(b.contains(e))){
                newSet.add(e);
            }
        }
        return newSet;
    }

    public static <T> Set symDifference(Set<T> a, Set<T> b){
        Set<T> newSet = new HashSet<>();
        for (T e : a){
            if (!(b.contains(e))){
                newSet.add(e);
            }
        }
        for (T x : b){
            if (!(a.contains(x))){
                newSet.add(x);
            }
        }
        return newSet;
    }
}
