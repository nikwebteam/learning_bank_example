package Collections_Practice;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class ValueComparator implements Comparator<Character> {

    private Map<Character, Integer> map = new HashMap<>();

    public ValueComparator(Map<Character, Integer> map){
        this.map = map;
    }

    @Override
    public int compare(Character o1, Character o2) {
         if (map.get(o1) >= map.get(o2)){
             return -1;
         }else {
             return 1;
         }
    }
}
