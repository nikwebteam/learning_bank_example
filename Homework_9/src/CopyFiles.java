import java.io.*;

public class CopyFiles {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String first = reader.readLine();
        String data = reader.readLine();
        reader.close();

        // ������� �� *
        RandomAccessFile raf = new RandomAccessFile(first, "r");
        RandomAccessFile rWrite = new RandomAccessFile(data, "rw");
        rWrite.skipBytes((int)rWrite.length());
        while (raf.read() != -1 ) {
            rWrite.write(raf.read());
        }
        raf.close();
        rWrite.close();

        // ������� �������
        FileInputStream is = new FileInputStream(data);
        FileOutputStream os = new FileOutputStream(first);

        int d = -1;
        while ((d = is.read()) != -1){
            os.write(d);
        }
        is.close();
        os.close();
    }
}
