import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class MinMaxBytes {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String data = reader.readLine();
        reader.close();

        FileInputStream is = new FileInputStream(data);
        int i = -1;

        Map<Character, Integer> map = new HashMap<>();
        while ((i = is.read()) != -1){
            char resu = (char)i;
            if (map.containsKey(resu)){
                map.put(resu, getValueByKey(map, resu) + 1);
            }else{
                map.put(resu, 1);
            }
        }
        is.close();
        char mi = ' ', ma = ' ';
        int min = 1,max = 1;
        for (Map.Entry<Character, Integer> pair : map.entrySet()){
            if (min >= pair.getValue()){
                min = pair.getValue();
                mi = pair.getKey();
            }
            if (max < pair.getValue()){
                max = pair.getValue();
                ma = pair.getKey();
            }
        }
        System.out.println("Minimum " + (byte)mi);
        System.out.println("Maximum " + (byte)ma);
    }
    public static int getValueByKey(Map<Character, Integer> map, Character key){
        for (Map.Entry<Character, Integer> pair : map.entrySet()){
            if (pair.getKey().equals(key)){
                return pair.getValue();
            }
        }
        return 0;
    }
}
