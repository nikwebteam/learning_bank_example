import java.io.*;
import java.util.Scanner;

public class ReverseLines {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String full = reader.readLine();
        String empty = reader.readLine();
        reader.close();

        Scanner sc = new Scanner(new File(full));
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(empty));

        while (sc.hasNextLine()){
            String tmp = sc.nextLine();
            fileWriter.write(new StringBuilder(tmp).reverse().toString());
            fileWriter.flush();
            fileWriter.newLine();
        }
        sc.close();
        fileWriter.close();
    }
}
