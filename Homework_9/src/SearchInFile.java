import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SearchInFile {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        // Read from console
        String file = reader.readLine();
        String word = reader.readLine();
        reader.close();
        // read from file and write string contains "word" in list
        Scanner scanner = new Scanner(new File(file));
        List<String> list = new ArrayList<>();
        while (scanner.hasNextLine()){
            String tmp = scanner.nextLine();
            if (tmp.contains(word)){
                list.add(tmp);
            }
        }
        scanner.close();

    }
}
