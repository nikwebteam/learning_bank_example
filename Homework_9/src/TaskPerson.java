import java.io.*;

public class TaskPerson {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Person person = new Person("Nikolay", "Chupikov", Sex.MALE);

        FileOutputStream fos = new FileOutputStream("first.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(person);
        oos.flush();
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("first.txt"));

        System.out.println(ois.readObject());

    }
    public static class Person implements Serializable{
        static final long serialVersionUID = 42L;
        transient String firstName;
        transient String lastName;
        private String fullName;
        final String finalString;
        Sex sex;
        transient PrintStream outputStream;

        Person(String firstName, String lastName,  Sex sex) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.fullName = String.format("%s, %s", lastName, firstName);
            this.finalString = "final string";
            this.sex = sex;
            this.outputStream = System.out;
        }

        @Override
        public String toString() {
            return firstName + " " +
                    lastName + ", " +
                    fullName + " " +
                    sex + " " +
                    finalString + "\n";
        }
    }

    enum Sex {
        MALE,
        FEMALE
    }
}

