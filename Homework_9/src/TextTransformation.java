import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TextTransformation {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String full = reader.readLine();
        String empty = reader.readLine();
        reader.close();

        Scanner scanner = new Scanner(new File(full));
        FileWriter fileWriter = new FileWriter(new File(empty));

        while (scanner.hasNextLine()){
            String tmp = scanner.next();
            if (tmp.length() %2 == 0){
                char first = Character.toUpperCase(tmp.charAt(0));
                fileWriter.append(first + tmp.substring(1) + ", ");
                fileWriter.flush();
            }
        }
        scanner.close();
        fileWriter.close();
    }
}
