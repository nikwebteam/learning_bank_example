import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class TotalWeight {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        // Read from console
        String file = reader.readLine();
        reader.close();

        Scanner scanner = new Scanner(new File(file));
        Map<String, Integer> list = new HashMap<>();
        while (scanner.hasNextLine()){
            String name = scanner.next();
            int weight = scanner.nextInt();
            if (list.containsKey(name)){
                int oldValue = list.get(name);
                list.put(name,oldValue + weight);
            }else {
                list.put(name, weight);
            }
        }
        scanner.close();
        int max = 0;
        for (Map.Entry<String, Integer> pair : list.entrySet()){
            if (pair.getValue() > max){
                max = pair.getValue();
            }
        }
        for (Map.Entry<String, Integer> pair : list.entrySet()){
            if (pair.getValue() == max){
                System.out.println(pair.getKey() + " " + max + " kg");
            }
        }
    }
}
